import { Component, OnInit, OnDestroy } from '@angular/core';
import {  Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { map, filter,  takeUntil, switchMap } from 'rxjs/operators';
import { LocationViewModel } from '../models/LocationViewModel';
import { LocationFacade } from '../services/facades/LocationFacade';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit, OnDestroy {
  locationItem: LocationViewModel;
  id: number;
  private onDestroy$ = new Subject();
  showForms = false;

  constructor(private route: ActivatedRoute, private objectFacade: LocationFacade, private router: Router) { }

  ngOnInit() {

   this.id = this.route.snapshot.params.id;
   this.showForms = this.route.snapshot.data.isLocationtExsists;

   this.objectFacade.isUpdating$(this.id).pipe(
          filter((isUpdate) => isUpdate === false),
          switchMap(() => this.objectFacade.get$(this.id)),
           takeUntil(this.onDestroy$))
           .subscribe(res => {
            if (res != null) {
              this.locationItem = res;
            }
          });

  }

  closeTab(e: Event) {
    this.router.navigate(['/']);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
