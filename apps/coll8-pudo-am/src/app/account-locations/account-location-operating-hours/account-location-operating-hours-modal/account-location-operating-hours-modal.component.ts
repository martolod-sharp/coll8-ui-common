import { PopupService } from './../../../popup/popup.service';
import { LocationOperatingHoursViewModel } from './../../../models/LocationOperatingHoursViewModel';
import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { FormEditorComponent } from '../../../shared/form-editor/form-editor.component';
import { FormGroup } from '@angular/forms';
import { LocationOperatingHoursFacade } from '../../../services/facades/LocationOperatingHoursFacade';

@Component({
  selector: 'app-account-location-operating-hours-modal',
  templateUrl: './account-location-operating-hours-modal.component.html',
  styleUrls: ['./account-location-operating-hours-modal.component.scss']
})
export class AccountLocationOperatingHoursModalComponent implements OnInit, OnDestroy {
  @Input() hour: LocationOperatingHoursViewModel = new LocationOperatingHoursViewModel(0, 0);
  @ViewChild(FormEditorComponent, { static: true }) childFormEditor: FormEditorComponent;
  hourForm: FormGroup;
  title: string;
  facade: LocationOperatingHoursFacade;


  constructor(
    private facadeInject: LocationOperatingHoursFacade,
    private fromBuilder: RxFormBuilder,
    private popupService: PopupService
  ) {
    this.facade = this.facadeInject;
  }


  ngOnInit() {
      this.hour = new LocationOperatingHoursViewModel(0, 0);
      this.hourForm = this.fromBuilder.formGroup(new LocationOperatingHoursViewModel(0, 0));
      this.hourForm.setValue(this.popupService.getCurrentModalParams());
  }

  close(data) {
    this.popupService.closePopup(null);
  }

  save(data) {
    this.popupService.closePopup(data);
  }

  ngOnDestroy(): void {

  }


}
