import { PopupRoutes } from './../../popup/popup-routes';
import { PopupService } from './../../popup/popup.service';
import { tap, mergeMap, filter, takeUntil, take, switchMap } from 'rxjs/operators';

import { LocationOperatingHoursListFacade } from './../../services/facades/LocationOperatingHoursListFacade';
import { Observable, Subscription, Subject } from 'rxjs';
import { LocationOperatingHoursViewModel } from './../../models/LocationOperatingHoursViewModel';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ClrDatagridStateInterface } from '@clr/angular';

@Component({
  selector: 'app-account-location-operating-hours',
  templateUrl: './account-location-operating-hours.component.html',
  styleUrls: ['./account-location-operating-hours.component.scss']
})
export class AccountLocationOperatingHoursComponent implements OnInit, OnDestroy {

  @Input() id: number;
  hoursList: Array<LocationOperatingHoursViewModel>;
  selected: LocationOperatingHoursViewModel;
  loading = true;
  isUpdating$: Observable<boolean>;
  total: number;
  private onDestroy$ = new Subject();

  listLoading: Subscription;
  listUpdate: Subscription;
  popupSubscription: Subscription;
  constructor(
    private hoursListFacade: LocationOperatingHoursListFacade,
    private popupService: PopupService
    ) {
  }


  ngOnInit() {
    this.isUpdating$ = this.hoursListFacade.isUpdating$(this.id).pipe(takeUntil(this.onDestroy$));
    this.hoursList = [];
  }

  refresh(state: ClrDatagridStateInterface<LocationOperatingHoursViewModel>) {
    this.reloadHours();
  }

  reloadHours() {
   const load$ = this.hoursListFacade.load$(this.id).pipe(take(1));
   this.loading = true;
   this.clearSubscriptions();
   this.listLoading = load$.pipe(
      mergeMap(() => this.isUpdating$, 1),
      filter((isUpdate) => isUpdate === false),
      tap((isUpdate) => this.loading = isUpdate),
      switchMap(() => this.hoursListFacade.get$(this.id)),
      takeUntil(this.onDestroy$)
      ).subscribe((res: LocationOperatingHoursViewModel[]) => this.hoursList = res);

  }

  openUpdatingHoursModal(hour: LocationOperatingHoursViewModel) {
    this.popupService.
    openPopup$<LocationOperatingHoursViewModel>(PopupRoutes.OperatingHoursEdit, `Operating Hours - ${hour.dayName}`, hour).
    pipe(
      filter((res) => res !== null),
      switchMap((res) => this.hoursListFacade.update$(res, this.id)),
      takeUntil(this.onDestroy$)
    ).
    subscribe(
      // todo: think what should be handle here
    );

  }


  clearSubscriptions() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  ngOnDestroy(): void {
    this.clearSubscriptions();
  }

}
