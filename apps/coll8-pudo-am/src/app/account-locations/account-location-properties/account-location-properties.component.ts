import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { LocationViewModel } from './../../models/LocationViewModel';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AccountLocationFacade } from './../../services/facades/AccountLocationFacade';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-account-location-properties',
  templateUrl: './account-location-properties.component.html',
  styleUrls: ['./account-location-properties.component.scss']
})
export class AccountLocationPropertiesComponent implements OnInit {

  @Input() location: LocationViewModel;
  locationForm: FormGroup;
  facade: AccountLocationFacade;

  constructor(private facadeInject: AccountLocationFacade, private fromBuilder: RxFormBuilder  ) {
    this.facade = this.facadeInject;
  }

  ngOnInit() {
    this.locationForm = this.fromBuilder.formGroup(new LocationViewModel(this.location.accountId));
   // this.locationForm.setValue(this.location);

  }

}
