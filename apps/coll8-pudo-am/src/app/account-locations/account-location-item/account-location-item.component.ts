import { BehaviorSubject, Subscription, of, Subject } from 'rxjs';
import { AccountLocationFacade } from './../../services/facades/AccountLocationFacade';
import { LocationViewModel } from './../../models/LocationViewModel';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { map, takeUntil } from 'rxjs/operators';
import { CanLeaveService } from '../../services/can-leave.service';
import { PopupService } from '../../popup/popup.service';
import { PopupRoutes } from '../../popup/popup-routes';

export enum LocationTabs { OpationHours, Propeties, Schedules}

@Component({
  selector: 'app-account-location-item',
  templateUrl: './account-location-item.component.html',
  styleUrls: ['./account-location-item.component.scss']
})
export class AccountLocationItemComponent implements OnInit, OnDestroy {

  locationTabs = LocationTabs;

  @Input() location: LocationViewModel;
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onDelete = new EventEmitter();

  activeTab = new BehaviorSubject<LocationTabs>(LocationTabs.Propeties);

  get diableChangeActiveTab$() {
    if (this.location === undefined) {
        return  of(false);
    }
    return this.canLeaveService.
    canLeave$([this.location.locationId], ['Account Locations', 'AccountLocationSchedules', 'Location Schedules'] )
    .pipe(map(value => !value));
  }

  private onDestroy$ = new Subject();

  constructor(
    private facade: AccountLocationFacade,
    private canLeaveService: CanLeaveService,
    private popupService: PopupService
  ) { }


  ngOnInit() {
  }

  changeTab(tab: LocationTabs) {
    if (this.diableChangeActiveTab$) {
      this.activeTab.next(tab);
    }
    return false;
  }

  delete() {
    this.popupService.openPopup$(PopupRoutes.DeleteConfirmation, 'Delete')
    .pipe(takeUntil(this.onDestroy$))
    .subscribe(res => {
      if (res !== null) {
        this.onDelete.emit(this.location);
      }
    });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
