import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { FormGroup } from '@angular/forms';
import { LocationViewModel } from './../../models/LocationViewModel';
import { AccountLocationFacade } from './../../services/facades/AccountLocationFacade';
import { Component, OnInit, Input } from '@angular/core';
import { PopupService } from '../../popup/popup.service';

@Component({
  selector: 'app-account-location-modal',
  templateUrl: './account-location-modal.component.html',
  styleUrls: ['./account-location-modal.component.scss']
})
export class AccountLocationModalComponent implements OnInit {

  @Input() accountId = 0;

  facade: AccountLocationFacade;

  locationForm: FormGroup;

  constructor(
    private facadeInject: AccountLocationFacade,
    private fromBuilder: RxFormBuilder,
    private popupService: PopupService
  ) {
    this.facade = this.facadeInject;
    const params = this.popupService.getCurrentModalParams();
    if (params) {
      this.accountId = params.accountId;
    }
  }
  ngOnInit() {
    this.locationForm = this.fromBuilder.formGroup(new LocationViewModel(this.accountId));
  }

  close() {
    this.locationForm.reset(new LocationViewModel(this.accountId));
    this.popupService.closePopup(null);
  }

  save(location: LocationViewModel) {
    this.popupService.closePopup(location);
  }

}
