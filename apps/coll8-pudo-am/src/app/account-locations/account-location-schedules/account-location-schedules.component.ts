import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { FormGroup, FormArray } from '@angular/forms';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { ClrDatagridStateInterface } from '@clr/angular';
import { filter, tap, take, takeUntil, switchMap } from 'rxjs/operators';
import { LocationSchedulesListFacade } from '../../services/facades/LocationSchedulesListFacade';
import { LocationScheduleFacade } from '../../services/facades/LocationScheduleFacade';
import { CanLeaveService } from '../../services/can-leave.service';
import { IErrorModel } from '../../models/Interfaces/IErrorModel';
import { LocationScheduleViewModel } from '../../models/LocationSchedule/LocationScheduleViewModel';
import { ScheduleType } from '../../models/LocationSchedule/ScheduleType';

@Component({
  selector: 'app-account-location-schedules',
  templateUrl: './account-location-schedules.component.html',
  styleUrls: ['./account-location-schedules.component.scss']
})
export class AccountLocationSchedulesComponent implements OnInit, OnDestroy {

  // Location ID
  @Input() id: number;
  loading = true;
  isUpdating$: Observable<boolean>;
  locationShedulerForm: FormGroup;
  btnDisabled = new BehaviorSubject<boolean>(true);
  private onDestroy$ = new Subject();
  constructor(
    public schedulesListFacade: LocationSchedulesListFacade,
    private scheduleFacade: LocationScheduleFacade,
    private canLeaveService: CanLeaveService,
    private formBuilder: RxFormBuilder
  ) {}

  ngOnInit() {

    this.isUpdating$ = this.schedulesListFacade.isUpdating$(this.id)
    .pipe(tap(isUpdating => console.log({isUpdating})), takeUntil(this.onDestroy$));

    this.locationShedulerForm = this.formBuilder.group({
      schedulesList: []
    });

  }

  onCheckboxChanged() {
    this.btnDisabled.next(false);
    this.setFormIsChanged(true);
  }

  updateSchedules() {
    this.schedulesList.value.forEach(schedule => {
      if (schedule !== null) {
        // Send entity update to the server
        this.scheduleFacade.update(schedule, schedule.id)
          .pipe(tap((err) => {
            if (err === null) {
              this.schedulesListFacade.update$(schedule, this.id);
            }
          }), filter(err => err !== null))
          .subscribe(err => this.handleError(err));
      }
    });

    this.btnDisabled.next(true);
    this.setFormIsChanged(false);
  }

  private setFormIsChanged(isChanged: boolean) {
    this.canLeaveService.setFormIsChanged('AccountLocationSchedules', this.id, isChanged);
  }

  handleError(model: IErrorModel) {
    // TODO: Notification service
    console.log({ model });
  }

  refresh(state: ClrDatagridStateInterface<LocationScheduleViewModel>) {
    this.reloadSchedules();
  }

  getScheduleTypeStr(value: number): string {
    return ScheduleType[value];
  }

  get schedulesList() { return (this.locationShedulerForm.get('schedulesList') as FormArray); }

  reloadSchedules() {
    this.isUpdating$.pipe(
       take(1),
       filter(isUpdating => isUpdating === false),
       switchMap(() => this.schedulesListFacade.load$(this.id).pipe(takeUntil(this.onDestroy$))),
       takeUntil(this.onDestroy$)
      ).subscribe(()=> console.log('upload complete'));


  }

  clearSubscriptions() {

  }

  cancel() {
    // Refresh changes, reload list
    this.reloadSchedules();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
