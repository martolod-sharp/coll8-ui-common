import { filter, mergeMap, takeUntil } from 'rxjs/operators';
import { concat, Subject } from 'rxjs';
import { LocationViewModel } from './../models/LocationViewModel';
import { AccountLocationListFacade } from './../services/facades/AccountLocationListFacade';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { PopupService } from '../popup/popup.service';
import { PopupRoutes } from '../popup/popup-routes';
import { CanLeaveService } from '../services/can-leave.service';

@Component({
  selector: 'app-account-locations',
  templateUrl: './account-locations.component.html',
  styleUrls: ['./account-locations.component.scss']
})
export class AccountLocationsComponent implements OnInit, OnDestroy {

  @Input() id: number;

  items: Array<LocationViewModel>;
  private onDestroy$ = new Subject();

  constructor(
    private locationListFasade: AccountLocationListFacade,
    private popupService: PopupService,
    private canLeaveService: CanLeaveService
  ) { }

  ngOnInit() {
    const stateUpdate$ = this.locationListFasade.isUpdating$(this.id).pipe(
      filter((isUpload) => isUpload === false),
      mergeMap(() => this.locationListFasade.get$(this.id)),
      takeUntil(this.onDestroy$)
    );
    const load$ = this.locationListFasade.load$(this.id).pipe(
      mergeMap(() => stateUpdate$),
      takeUntil(this.onDestroy$)
    );
    concat(load$, stateUpdate$)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(items => this.items = items);

  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  addLocation(location: LocationViewModel) {
    if (location != null) {
      this.locationListFasade.add$(location, this.id);
    }
  }

  updateLocations(location: LocationViewModel) {
    this.locationListFasade.remove$(location, this.id);
  }

  createLocation() {
    this.popupService.
    openPopup$<LocationViewModel>(PopupRoutes.LocationAdd, 'Add Location', {accountId: this.id}).
    pipe(takeUntil(this.onDestroy$)).
    subscribe((location) => this.addLocation(location));
  }

}
