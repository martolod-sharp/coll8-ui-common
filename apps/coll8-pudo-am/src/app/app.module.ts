import { ModalConfirmModule } from './shared/modal-confirm/modal-confirm.module';
import { FormCollectionEditorComponent } from './shared/form-editor/FormCollectionEditorComponent';
import { LocationGuardService } from './services/guards/location-guard.service';
import { AccountGuardService } from './services/guards/account-guard.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ClarityModule } from '@clr/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountSelectorComponent } from './account-selector/account-selector.component';
import { ModalEditorComponent } from './shared/modal-editor/modal-editor.component';
import { AccountEditorComponent } from './account-editor/account-editor.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { AccountAddressComponent } from './account-address/account-address.component';
import { AccountContactsComponent } from './account-contacts/account-contacts.component';
import { AccountContactComponent } from './account-contacts/account-contact/account-contact.component';
import { AccountLocationsComponent } from './account-locations/account-locations.component';
import { LocationContactsComponent } from './location-contacts/location-contacts.component';
import { LocationContactComponent } from './location-contacts/location-contact/location-contact.component';
import { FormEditorComponent } from './shared/form-editor/form-editor.component';
import { EntityStateWatchComponent } from './shared/entity-state-watch/entity-state-watch.component';
import { AddAccountFormComponent } from './account-selector/add-account-form/add-account-form.component';
import { AccountDetailsFormComponent } from './account-details/account-details-form/account-details-form.component';

import { AccountAddressFormComponent } from './account-address/account-address-form/account-address-form.component';
// tslint:disable-next-line: max-line-length
import { EditAccountContactFormComponent } from './account-contacts/account-contact/edit-account-contact-form/edit-account-contact-form.component';
// tslint:disable-next-line: max-line-length
import { EditLocationContactFormComponent } from './location-contacts/location-contact/edit-location-contact-form/edit-location-contact-form.component';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InputControlComponent } from './shared/input-control/input-control.component';
import { SelectControlComponent } from './shared/select-control/select-control.component';
import { ClickOutSideDirective } from './shared/click-out-side.directive';
import { ModalConfirmDeletionComponent } from './shared/modal-confirm-deletion/modal-confirm-deletion.component';
import { AccountLocationItemComponent } from './account-locations/account-location-item/account-location-item.component';
import { AccountLocationModalComponent } from './account-locations/account-location-modal/account-location-modal.component';
import { AccountLocationPropertiesComponent } from './account-locations/account-location-properties/account-location-properties.component';
// tslint:disable-next-line: max-line-length
import { AccountLocationOperatingHoursComponent } from './account-locations/account-location-operating-hours/account-location-operating-hours.component';
// tslint:disable-next-line: max-line-length
import { AccountLocationOperatingHoursModalComponent } from './account-locations/account-location-operating-hours/account-location-operating-hours-modal/account-location-operating-hours-modal.component';
import { BaseContactComponent } from './shared/base-contact/base-contact.component';
import { BaseContactItemComponent } from './shared/base-contact-item/base-contact-item.component';
import { BaseEditContactFormComponent } from './shared/base-edit-contact-form/base-edit-contact-form.component';
import { AddContactComponent } from './shared/add-contact/add-contact.component';
import { MsalModule, MsalInterceptor } from '@azure/msal-angular';
import { AccountComponent } from './account/account.component';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { AuthInterceptor } from './services/AuthInterceptor';
import { LocationSelectorComponent } from './location-selector/location-selector.component';
import { AccountLocationSchedulesComponent } from './account-locations/account-location-schedules/account-location-schedules.component';
import { HomeMenuComponent } from './home-menu/home-menu.component';

import { LocationComponent } from './location/location.component';
import { PopupModule } from './popup/popup.module';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    AccountSelectorComponent,
    ModalEditorComponent,
    AccountEditorComponent,
    AccountDetailsComponent,
    AccountAddressComponent,
    AccountContactsComponent,
    AccountContactComponent,
    AccountLocationsComponent,
    LocationContactsComponent,
    LocationContactComponent,
    FormEditorComponent,
    FormCollectionEditorComponent,
    EntityStateWatchComponent,
    AddAccountFormComponent,
    AccountDetailsFormComponent,
    AccountAddressFormComponent,
    EditAccountContactFormComponent,
    EditLocationContactFormComponent,
    InputControlComponent,
    SelectControlComponent,
    ClickOutSideDirective,
    ModalConfirmDeletionComponent,
    AccountLocationItemComponent,
    AccountLocationModalComponent,
    BaseContactComponent,
    BaseContactItemComponent,
    BaseEditContactFormComponent,
    AccountLocationPropertiesComponent,
    AccountLocationOperatingHoursComponent,
    AccountLocationOperatingHoursModalComponent,
    ModalConfirmDeletionComponent,
    AccountComponent,
    AddContactComponent,
    LocationSelectorComponent,
    AccountLocationSchedulesComponent,
    HomeMenuComponent,
    AddContactComponent,
    LocationComponent,
  ],
  entryComponents: [
    AddAccountFormComponent,
    EditAccountContactFormComponent,
    AccountLocationModalComponent,
    AccountLocationOperatingHoursModalComponent,
    EditLocationContactFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    ModalConfirmModule,
    BrowserAnimationsModule,
    ClarityModule,
    AppRoutingModule,
    PopupModule,
    MsalModule.forRoot({
      clientID: environment.adClientId,
      authority: 'https://login.microsoftonline.com/common/',
      redirectUri: environment.adRedirectUri,
      cacheLocation: environment.adCacheLocation,
      navigateToLoginRequestUrl: true,
      popUp: false,
      consentScopes: ['openid', 'offline_access', environment.adClientScope],
      protectedResourceMap: [[environment.baseUrl, ['openid', 'offline_access', environment.adClientScope]]]
    })
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  },
  AccountGuardService,
  LocationGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
