import { Component, OnInit } from '@angular/core';
import { BaseContactComponent } from '../shared/base-contact/base-contact.component';
import { LocationContactsListFacade } from '../services/facades/LocationContactsListFacade';
import { PopupService } from '../popup/popup.service';
import { CanLeaveService } from '../services/can-leave.service';

@Component({
  selector: 'app-location-contacts',
  templateUrl: '../shared/base-contact/base-contact.component.html',
  styleUrls: ['../shared/base-contact/base-contact.component.scss'],
})
export class LocationContactsComponent extends BaseContactComponent{
  constructor(contactsListFacade: LocationContactsListFacade, popupService: PopupService, canLeaveService: CanLeaveService) {
    super(contactsListFacade, popupService, canLeaveService);
    this.type = 'location';
  }
}
