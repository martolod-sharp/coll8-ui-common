import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationContactComponent } from './location-contact.component';

describe('LocationContactComponent', () => {
  let component: LocationContactComponent;
  let fixture: ComponentFixture<LocationContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
