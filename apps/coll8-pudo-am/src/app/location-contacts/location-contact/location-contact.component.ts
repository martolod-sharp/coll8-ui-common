import { Component } from '@angular/core';
import { LocationContactFacade } from '../../services/facades/LocationContactFacade';
import { BaseContactItemComponent } from '../../shared/base-contact-item/base-contact-item.component';
import { PopupService } from '../../popup/popup.service';

@Component({
  selector: 'app-location-contact',
  templateUrl: '../../shared/base-contact-item/base-contact-item.component.html',
  styleUrls: ['../../shared/base-contact-item/base-contact-item.component.scss']
})
export class LocationContactComponent extends BaseContactItemComponent {
  constructor(
    facade: LocationContactFacade,
    popupService: PopupService
  ) {
    super(facade, popupService);
  }
}
