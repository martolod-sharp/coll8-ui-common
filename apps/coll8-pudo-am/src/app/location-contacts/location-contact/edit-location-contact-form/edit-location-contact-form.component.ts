import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { Component } from '@angular/core';
import { BaseEditContactFormComponent } from '../../../shared/base-edit-contact-form/base-edit-contact-form.component';
import { LocationContactFacade } from '../../../services/facades/LocationContactFacade';
import { PopupService } from '../../../popup/popup.service';
import { LocationContactViewModel } from '../../../models/LocationContactViewModel';


@Component({
  selector: 'app-edit-location-contact-form',
  templateUrl: '../../../shared/base-edit-contact-form/base-edit-contact-form.component.html',
  styleUrls: ['../../../shared/base-edit-contact-form/base-edit-contact-form.component.scss']
})
export class EditLocationContactFormComponent extends BaseEditContactFormComponent {
  formName = 'Account Locaiton Contacts';
  constructor(
    facade: LocationContactFacade,
    formBuilder: RxFormBuilder,
    protected popupService: PopupService
  ) {
    super(facade, formBuilder, popupService);
    this.buildModel = new LocationContactViewModel(0);

    const params = this.popupService.getCurrentModalParams();
    if (params) {
      this.id = params.id;
      this.accountId = params.accountId;
    }
  }
}
