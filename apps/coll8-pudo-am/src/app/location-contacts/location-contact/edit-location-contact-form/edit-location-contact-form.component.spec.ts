import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLocationContactFormComponent } from './edit-location-contact-form.component';

describe('EditLocationContactFormComponent', () => {
  let component: EditLocationContactFormComponent;
  let fixture: ComponentFixture<EditLocationContactFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditLocationContactFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLocationContactFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
