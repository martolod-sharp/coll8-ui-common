import { required, maxLength, prop, latitude, longitude, pattern } from '@rxweb/reactive-form-validators';

export class LocationViewModel {
  @prop()
  locationId: number;

  @required()
  @maxLength({value: 50})
  companyName: string;

  @required()
  @maxLength({value: 15})
  locationCode: string;

  @required()
  @maxLength({value: 100})
  address1: string;

  @maxLength({value: 100})
  address2: string;

  @maxLength({value: 100})
  address3: string;

  @required()
  @maxLength({value: 60})
  cityTown: string;

  @required()
  @maxLength({value: 15})
  postCode: string;

  @required()
  @maxLength({value: 60})
  county: string;

  @required()
  @maxLength({value: 60})
  country: string;

  @pattern({expression:{phone: /^((\+\d)+([0-9\(\)\-])+)$/}})
  @required()
  @maxLength({value: 30})
  phone: string;

  @required()
  international: boolean;

  @longitude()
  @maxLength({value: 30})
  longitude: string;

  @latitude()
  @maxLength({value: 30})
  latitude: string;

  @maxLength({value: 500})
  storeUrl: string;

  @prop()
  accountId: number;


  constructor(accountId: number) {
    this.locationId = 0;
    this.international = false;
    this.accountId = accountId;
  }
}
