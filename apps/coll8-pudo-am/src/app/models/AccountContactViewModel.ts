import { IContactViewModel } from './Interfaces/IContactViewModel';
import { required, maxLength, email, prop, pattern } from '@rxweb/reactive-form-validators';

export class AccountContactViewModel implements IContactViewModel {
  @prop()
  contactId = 0;

  @prop()
  accountId: number;

  @required()
  @maxLength({value: 15})
  title: string;

  @required()
  @maxLength({value: 60})
  firstName: string;

  @required()
  @maxLength({value: 60})
  surname: string;


  @maxLength({value: 60})
  jobTitle: string;

  @required()
  @maxLength({value: 60})
  department: string;

  @pattern({expression:{phone: /^((\+\d)+([0-9\(\)\-])+)$/}})
  @required()
  @maxLength({value: 20})
  phone: string;

  @pattern({expression:{phone: /^((\+\d)+([0-9\(\)\-])+)$/}})
  @required()
  @maxLength({value: 20})
  mobile: string;

  @email()
  @required()
  @maxLength({value: 60})
  email: string;

  constructor(accountId: number) {
    this.accountId = accountId;
  }

  newWithParent(masterId: number) {
    return new AccountContactViewModel(masterId);
   }
}
