export class LocationOperatingHoursUpdateViewModel {
  LocationOperatingHoursId: number;
  DayType: string;
  OpenFrom: string;
  OpenTo: string;
}
