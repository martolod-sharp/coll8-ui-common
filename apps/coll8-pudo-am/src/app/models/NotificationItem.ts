export enum NotificationType {Info, Warning, Success, Danger}

export class NotificationItem implements Error {
  name = '';
  message = '';
  type = NotificationType.Info;
  stack?: string;

  constructor(type: NotificationType, message: string, name?: string) {
    this.type = type;
    this.message = message;
    if (name !== undefined) {
      this.name = name;
    }
  }
}
