export class VatRateViewModel {
  vatRateId: number;
  countryId: number;
  vatCode: string;
  description: string;
  vatRate1: number;
  activeFrom: any;
  activeTo: any;
}
