export class LocationTableViewModel {
    locationId: number;
    companyName: string;
    locationCode: string;
    addresses: string;
    cityTown: string;
    postCode: string;
    county: string;
    country: string;
}
