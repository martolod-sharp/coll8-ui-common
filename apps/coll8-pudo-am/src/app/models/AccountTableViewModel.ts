export class AccountTableViewModel {
  accountId: number;
  accountCode: string;
  accountName: string;
  addresses: string;
  cityTown: string;
  county: string;
  postCode: string;
  country: string;
}
