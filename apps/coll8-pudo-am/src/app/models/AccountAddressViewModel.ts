import { required, maxLength, email, prop, pattern } from '@rxweb/reactive-form-validators';

export class AccountAddressViewModel {
  @prop()
  addressId = 0;
  @prop()
  accountId = 0;

  @required()
  @maxLength({value: 50})
  companyName: string;

  @required()
  @maxLength({value: 100})
  address1: string;

  @maxLength({value: 100})
  address2: string;

  @maxLength({value: 100})
  address3: string;

  @required()
  @maxLength({value: 60})
  cityTown: string;

  @required()
  @maxLength({value: 15})
  postCode: string;

  @required()
  @maxLength({value: 60})
  county: string;

  @required()
  @maxLength({value: 60})
  country: string;

  @pattern({expression:{phone: /^((\+\d)+([0-9\(\)\-])+)$/}})
  @required()
  @maxLength({value: 30})
  phone: string;

  @email()
  @required()
  @maxLength({value: 60})
  email: string;

  @required()
  international: boolean;

  constructor(accountId) {
    this.accountId = accountId;
  }
}
