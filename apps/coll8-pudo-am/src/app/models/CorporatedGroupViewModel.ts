export class CorporatedGroupViewModel {
  corporatedGroupId: number;
  name: string;
  code: string;
}
