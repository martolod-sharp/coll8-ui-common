import { required, maxLength, prop } from '@rxweb/reactive-form-validators';

export class AccountCreateViewModel {
  @prop()
  accountId = 0;

  @required()
  corporatedGroupId: number;

  @required()
  @maxLength({value: 15})
  accountCode: string;

  @required()
  @maxLength({value: 50})
  accountName: string;

  // constructor() {
  //   this.accountCode = '';
  //   this.accountName = '';
  //   this.corporatedGroupId = 0;
  // }
}
