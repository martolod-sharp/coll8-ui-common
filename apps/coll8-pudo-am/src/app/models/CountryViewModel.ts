export class CountryViewModel {
  countryId: number;
  countryCode: string;
  description: string;
}
