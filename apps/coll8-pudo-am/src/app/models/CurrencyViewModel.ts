export class CurrencyViewModel {
  currencyId: number;
  currencyCode: string;
  country: string;
  description: string;
}
