
export interface IErrorModel {
  errors: {[key: string]: string[]};
}
export class ErrorModel implements IErrorModel {
  errors: { [key: string]: string[]; };
}
