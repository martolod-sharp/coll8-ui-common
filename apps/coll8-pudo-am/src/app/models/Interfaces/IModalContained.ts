export interface IModalContained {
  handleModalOpen(): void;
  handleModalClose(): void;
  handleModalCloseByCross(): void;
}
