export interface IContactViewModel{
  contactId: number;
  title: string;
  firstName: string;
  surname: string;
  jobTitle: string;
  department: string;
  phone: string;
  mobile: string;
  email: string;
  newWithParent(masterId: number): any;
}
