import { ScheduleType } from './ScheduleType';

export class LocationScheduleViewModel {
    id: number;
    locationId: number;
    scheduleType: ScheduleType;
    sunday: boolean;
    monday: boolean;
    tuesday: boolean;
    wednesday: boolean;
    thursday: boolean;
    friday: boolean;
    saturday: boolean;
}
