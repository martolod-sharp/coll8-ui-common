export class BaseListViewModel<T>
	{
		page: number;
		perPage: number;
		total: number;
		resultList: T[];
	}