export class LocationCreateViewModel {
  AccountId: number;
  CompanyName: string;
  LocationCode: string;
  Address1: string;
  Address2: string;
  Address3: string;
  CityTown: string;
  PostCode: string;
  County: string;
  Country: string;
  Phone: string;
  International: boolean;
  Longitude: string;
  Latitude: string;
  StoreUrl: string;
}
