import {  prop, time } from '@rxweb/reactive-form-validators';

export class LocationOperatingHoursViewModel {
  @prop()
  locationOperatingHoursId: number;
  @prop()
  locationId: number;
  @prop()
  dayName: string;
  @prop()
  dayType: string;
  @time()
  openFrom: string;
  @time()
  openTo: string;

  constructor(locationId: number, locationOperatingHoursId?: number) {
    this.locationId = locationId;
    this.locationOperatingHoursId = locationOperatingHoursId;
    this.dayName = '';
    this.dayType = '';
    this.openFrom = '';
    this.openTo = '';
  }
}
