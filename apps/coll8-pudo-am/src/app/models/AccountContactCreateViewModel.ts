export class AccountContactCreateViewModel {
  AccountId: number;
  Title: string;
  FirstName: string;
  Surname: string;
  JobTitle: string;
  Department: string;
  Phone: string;
  Mobile: string;
  Email: string;
}
