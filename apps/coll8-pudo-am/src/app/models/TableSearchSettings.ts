export class TableSearchSettings {
  Pagination: TablePager;
  Filter: TableSimpleFilter[];
  Sort: TableSort[];
}

export class TablePager {
  constructor(
    public Page: number = 0,
    public PerPage: number = 10) { }
}

export class TableSimpleFilter {

  Field: string;
  CompareType: FilterType;
  Value1: any;
  Value2: any;
}

export enum FilterType {
  DateFrom = 0,
  DateFromIncl = 1,
  DateFor = 2,
  DateForIncl = 3,
  DateIn = 4,
  DateInIncl = 5,
  Contains = 6,
  SameString = 7,
  SameInt = 8,
  TriState = 9,
  Lookup = 10,
  DateTimeFrom = 11,
  DateTimeFromIncl = 12,
  DateTimeFor = 13,
  DateTimeForIncl = 14,
  DateTimeIn = 15,
  DateTimeInIncl = 16
}

export class TableSort{
  Field: string;
  SortDirection: string;
}