import { required, email, maxLength, prop, pattern } from '@rxweb/reactive-form-validators';
import { IContactViewModel } from './Interfaces/IContactViewModel';

export class LocationContactViewModel implements IContactViewModel {
  @prop()
  contactId: number;
  @prop()
  locationId: number;

  @required()
  @maxLength({value: 15})
  title: string;
  @required()
  @maxLength({value: 60})
  firstName: string;
  @required()
  @maxLength({value: 60})
  surname: string;


  @maxLength({value: 60})
  jobTitle: string;

  @required()
  @maxLength({value: 60})
  department: string;

  @pattern({expression: {phone: /^((\+\d)+([0-9\(\)\-])+)$/}})
  @required()
  @maxLength({value: 20})
  phone: string;

  @pattern({expression: {phone: /^((\+\d)+([0-9\(\)\-])+)$/}})
  @required()
  @maxLength({value: 20})
  mobile: string;

  @email()
  @required()
  @maxLength({value: 60})
  email: string;

  constructor(accountId: number) {
    this.contactId = 0;
    this.locationId = accountId;
  }


  newWithParent(masterId: number) {
    return new LocationContactViewModel(masterId);
  }
}
