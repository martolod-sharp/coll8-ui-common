import { required, maxLength, prop } from '@rxweb/reactive-form-validators';

export class AccountViewModel {
  @prop()
  accountId: number;

  @required()
  corporatedGroupId: number;

  @required()
  @maxLength({value: 15})
  accountCode: string;

  @maxLength({value: 50})
  accountName: string;

  @required()
  @maxLength({value: 15})
  companyRegNo: string;

  @required()
  @maxLength({value: 15})
  vatno: string;

  @required()
  vatexempt: boolean;

  @required()
  @maxLength({value: 15})
  vatexemptAuthoriseNo: string;

  @required()
  @maxLength({value: 15})
  eori: string;

  @prop()
  tradingCurrencyId: number;

  @required()
  tradingCountryId: number;

  @required()
  tradingVatRateId: number;


}
