import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { AccountContactFacade } from './../../../services/facades/AccountContactFacade';

import { AccountContactViewModel } from './../../../models/AccountContactViewModel';
import { Component } from '@angular/core';
import { BaseEditContactFormComponent } from '../../../shared/base-edit-contact-form/base-edit-contact-form.component';
import { PopupService } from '../../../popup/popup.service';


@Component({
  selector: 'app-edit-account-contact-form',
  templateUrl: '../../../shared/base-edit-contact-form/base-edit-contact-form.component.html',
  styleUrls: ['../../../shared/base-edit-contact-form/base-edit-contact-form.component.scss']
})
export class EditAccountContactFormComponent extends BaseEditContactFormComponent {
  constructor(
    facade: AccountContactFacade,
    formBuilder: RxFormBuilder,
    popupService: PopupService
  ) {
    super(facade, formBuilder, popupService);
    this.buildModel = new AccountContactViewModel(0);

    const params = this.popupService.getCurrentModalParams();
    if (params) {
      console.log(params.id);
      this.id = params.id;
      this.accountId = params.accountId;
    }
  }
}
