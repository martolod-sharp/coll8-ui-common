import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAccountContactFormComponent } from './edit-account-contact-form.component';

describe('EditAccountContactFormComponent', () => {
  let component: EditAccountContactFormComponent;
  let fixture: ComponentFixture<EditAccountContactFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAccountContactFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAccountContactFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
