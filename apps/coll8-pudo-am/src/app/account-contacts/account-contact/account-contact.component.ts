import { AccountContactFacade } from './../../services/facades/AccountContactFacade';
import { Component } from '@angular/core';
import { BaseContactItemComponent } from '../../shared/base-contact-item/base-contact-item.component';
import { PopupService } from '../../popup/popup.service';


@Component({
  selector: 'app-account-contact',
  templateUrl: '../../shared/base-contact-item/base-contact-item.component.html',
  styleUrls: ['../../shared/base-contact-item/base-contact-item.component.scss']
})
export class AccountContactComponent extends BaseContactItemComponent {
  constructor(
    facade: AccountContactFacade,
    popupService: PopupService
  ) {
    super(facade, popupService);
  }
}
