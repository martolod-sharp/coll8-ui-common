import { Component, OnInit, Input } from '@angular/core';
import { ContactsListFacade } from '../services/facades/ContactsListFacade';
import { BaseContactComponent } from '../shared/base-contact/base-contact.component';
import { PopupService } from '../popup/popup.service';
import { CanLeaveService } from '../services/can-leave.service';

@Component({
  selector: 'app-account-contacts',
  templateUrl: '../shared/base-contact/base-contact.component.html',
  styleUrls: ['../shared/base-contact/base-contact.component.scss'],
})
export class AccountContactsComponent extends BaseContactComponent {
  constructor(contactsListFacade: ContactsListFacade,  popupService: PopupService, canLeaveService: CanLeaveService) {
    super(contactsListFacade,  popupService, canLeaveService);
    this.type = 'account';
  }
}
