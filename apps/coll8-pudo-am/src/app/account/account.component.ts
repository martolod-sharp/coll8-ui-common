import { Subscription, BehaviorSubject, Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit, OnDestroy {
  id: number;
  routeSubscribe: Subscription;
  showForms = false;
  private onDestroy$ = new Subject();
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
     this.id = this.route.snapshot.params.id;
     this.showForms = this.route.snapshot.data.isAccountExsists;
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
