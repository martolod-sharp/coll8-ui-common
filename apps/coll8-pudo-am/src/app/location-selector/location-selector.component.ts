import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription, concat } from 'rxjs';
import { LocationTableViewModel } from '../models/LocationTableViewModel';
import { TableSearchSettings, TablePager, FilterType } from '../models/TableSearchSettings';
import { LocationTableListFacade } from '../services/facades/LocationTableListFacade';
import { LocationExportFacade } from '../services/facades/LocationExportFacade';
import { ExportTypesEnum } from '../models/ExportTypesEnum';
import { ClrDatagridStateInterface } from '@clr/angular';
import { LocationListViewModel } from '../models/LocationListViewModel';
import { filter, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-location-selector',
  templateUrl: './location-selector.component.html',
  styleUrls: ['./location-selector.component.scss']
})
export class LocationSelectorComponent implements OnInit, OnDestroy {
  locationsList: Array<LocationTableViewModel>;
  selected: LocationTableViewModel;
  searchSettings: TableSearchSettings;
  loading = true;
  fileName: string;
  isUpdating$: Observable<boolean>;
  isUpdatingExport$: Observable<boolean>;
  total: number;
  exportCompleted = true;


  exportSubscription: Subscription;
  loadSubscription: Subscription;

  constructor(private locationsListFacade: LocationTableListFacade, private locationExportFacade: LocationExportFacade) {
  }

  ngOnInit() {

    this.isUpdating$ = this.locationsListFacade.isUpdating$(0);
    this.isUpdatingExport$ = this.locationExportFacade.isUpdating$(0);

    this.searchSettings = {
      Pagination: new TablePager(),
      Sort: null,
      Filter: null
    };
    this.locationsList = [];
  }


  downloadExportFile(event: any) {
    this.exportCompleted = false;
    const load$ = this.locationExportFacade.loadFile(ExportTypesEnum.Xls, 0);
    const stateUpdate$ = this.isUpdatingExport$.pipe(
      filter(isUpdate => isUpdate === false && this.exportCompleted === false),
      mergeMap(() => this.locationExportFacade.get$(0)));
    if (this.exportSubscription !== undefined) {
      this.exportSubscription.unsubscribe();
    }
    this.exportSubscription = concat(load$, stateUpdate$).subscribe((blob: Blob) => {
      // Prevent duplicates
      this.exportCompleted = true;
      const now = new Date();
      const fileName = `Locations List ${now.toLocaleString()}.xls`;
      const objectUrl: string = window.URL.createObjectURL(blob);
      const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;

      a.href = objectUrl;
      a.download = fileName;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      window.URL.revokeObjectURL(objectUrl);
    });
  }

  refresh(state: ClrDatagridStateInterface<LocationTableViewModel>) {
    // TODO: Sorting for second part of development flow

    this.searchSettings.Filter = [];
    if (state.filters) {
      for (const filteredItem of state.filters) {
        const { property, value } = filteredItem as {
          property: string;
          value: string;
        };
        this.searchSettings.Filter.push({
          // TODO: Another options
          CompareType: FilterType.Contains,
          Field: property,
          Value1: value,
          Value2: null
        });
      }
    }

    this.searchSettings.Pagination = {
      Page: state.page.current - 1,
      PerPage: state.page.size
    };

    this.reloadLocations();
  }

  reloadLocations() {

    const load$ = this.locationsListFacade.load(this.searchSettings, 0);
    const stateUpdate$ = this.isUpdating$.pipe(filter((isUpdate) => isUpdate === false), mergeMap(() => this.locationsListFacade
      .get$(0)));
    this.loading = true;
    if (this.loadSubscription !== undefined) {
      this.loadSubscription.unsubscribe();
    }
    this.loadSubscription = concat(load$, stateUpdate$)
      .subscribe((res: LocationListViewModel) => {
        this.locationsList = res.resultList;
        this.total = res.total;
        this.loading = false;
      });
  }


  selectedChanged() {
    const url = `/location/${this.selected.locationId}`;
    window.open(url, '_blank');
  }

  clearSubsciprions() {
    if (this.loadSubscription !== undefined) {
      this.loadSubscription.unsubscribe();
    }
    if (this.exportSubscription !== undefined) {
      this.exportSubscription.unsubscribe();
    }
  }

  ngOnDestroy(): void {
    this.clearSubsciprions();
  }

}
