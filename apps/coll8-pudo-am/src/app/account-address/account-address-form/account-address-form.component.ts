
import { AccountAddressFacade } from './../../services/facades/AccountAddressFacade';

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { AccountAddressViewModel } from '../../models/AccountAddressViewModel';

@Component({
  selector: 'app-account-address-form',
  templateUrl: './account-address-form.component.html',
  styleUrls: ['./account-address-form.component.scss']
})
export class AccountAddressFormComponent implements OnInit {
  @Input() id: number;
  addressForm: FormGroup;
  accountAddressFacade: AccountAddressFacade;

  constructor(
    private accountAddressFacadeInject: AccountAddressFacade,
    private fromBuilder: RxFormBuilder
  ) {
    this.addressForm = this.fromBuilder.formGroup(new AccountAddressViewModel(this.id));
    this.accountAddressFacade = this.accountAddressFacadeInject;
  }

  ngOnInit() {
  }
}
