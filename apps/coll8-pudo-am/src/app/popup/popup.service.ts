import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { Subject, Observable, of } from 'rxjs';
import { PopupRoutes, PopupBaseRoute } from './popup-routes';
import { CanLeaveService } from '../services/can-leave.service';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  private title: string = null;
  private params: any = null;
  private modalType: PopupRoutes = null;

  private popupCloseEvent$: Subject<any | null> = null;

  constructor(
    private router: Router,
    private canLeaveService: CanLeaveService
  ) {}

  public getCurrentModalParams = () => this.params;
  public getCurrentModalTitle = () => this.title;
  public getCurrentModalType = () => this.modalType;

  public openPopup$<T>(route: PopupRoutes, title: string = null, params: any = null): Observable<T> {
    if (!this.canLeaveService.canLeave) {
      return of(null);
    }
    this.title = title;
    this.params = params;
    this.modalType = route;
    this.popupCloseEvent$ = new Subject<any | null>();
    this.router.navigate(
      [{ outlets: { popup: `${PopupBaseRoute}/${route}` }}],
      { skipLocationChange: true }
    );

    return this.popupCloseEvent$.pipe(take(1));
  }

  public closePopup(model: any = null) {
    this.title = null;
    this.params = null;
    this.modalType = null;

    this.router.navigate(
      [{ outlets: { popup: null }}],
      { skipLocationChange: true }
    );

    if (this.popupCloseEvent$ !== null) {
      this.popupCloseEvent$.next(model);
      this.popupCloseEvent$.complete();
      this.popupCloseEvent$ = null;
    }
  }

}
