import { Component, OnInit } from '@angular/core';
import { PopupService } from './popup.service';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { PopupRoutes } from './popup-routes';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {

  public title = this.popupService.getCurrentModalTitle();
  public isClosable = true; //this.popupService.getCurrentModalType() !== PopupRoutes.ConfirmFormLeave;

  constructor(
    private popupService: PopupService,
    public route: ActivatedRoute
  ) { }

  ngOnInit() {
    console.log('Init modal', this.route);
  }

  onCloseModal() {
    this.popupService.closePopup();
  }

}
