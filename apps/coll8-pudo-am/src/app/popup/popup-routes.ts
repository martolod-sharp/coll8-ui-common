export const PopupBaseRoute = 'popup';

export enum PopupRoutes {
  AddAccount = 'add-account',
  AccountContactAdd = 'account-contact-add',
  AccountContactEdit = 'account-contact-edit',
  LocationAdd = 'location-add',
  OperatingHoursEdit = 'operation-hours-edit',
  LocationContactAdd = 'location-contact-add',
  LocationContactEdit = 'location-contact-edit',
  DeleteConfirmation = 'delete-confirmation',
}
