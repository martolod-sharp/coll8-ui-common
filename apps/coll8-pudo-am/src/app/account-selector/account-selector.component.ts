import { filter, mergeMap, takeUntil } from 'rxjs/operators';
import { AccountTableViewModel } from '../models/AccountTableViewModel';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, concat, BehaviorSubject, Subject } from 'rxjs';
import { ClrDatagridStateInterface } from '@clr/angular';
import {
  TableSearchSettings,
  FilterType,
  TablePager
} from '../models/TableSearchSettings';
import { AccountListViewModel } from '../models/AccountListViewModel';
import { AccountTableListFacade } from '../services/facades/AccountTableListFacade';
import { AccountExportFacade } from '../services/facades/AccountExportFacade';
import { ExportTypesEnum } from '../models/ExportTypesEnum';
import { PopupService } from '../popup/popup.service';
import { PopupRoutes } from '../popup/popup-routes';

@Component({
  selector: 'app-account-selector',
  templateUrl: './account-selector.component.html',
  styleUrls: ['./account-selector.component.scss']
})
export class AccountSelectorComponent implements OnInit, OnDestroy {
  accountsList: Array<AccountTableViewModel>;
  selected: AccountTableViewModel;
  selectedSubject: BehaviorSubject<AccountTableViewModel> = new BehaviorSubject<AccountTableViewModel>(new AccountTableViewModel());
  searchSettings: TableSearchSettings;
  loading = true;
  fileName: string;
  isUpdating$: Observable<boolean>;
  isUpdatingExport$: Observable<boolean>;
  total: number;
  addAccountModal = false;
  exportCompleted = true;

  private onDestroy$ = new Subject();

  constructor(
    private accountsListFacade: AccountTableListFacade,
    private accountExportFacade: AccountExportFacade,
    private popupService: PopupService
  ) {
  }

  ngOnInit() {
    this.isUpdating$ = this.accountsListFacade.isUpdating$(0);
    this.isUpdatingExport$ = this.accountExportFacade.isUpdating$(0);

    this.searchSettings = {
      Pagination: new TablePager(),
      Sort: null,
      Filter: null
    };
    this.accountsList = [];
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  addAccount() {
    const params = new AccountTableViewModel();
    this.popupService.openPopup$(PopupRoutes.AddAccount, 'Add Account', params).pipe(takeUntil(this.onDestroy$)).subscribe(
      (res) => this.onAddAccountOk(res)
    );
  }

  downloadExportFile(event: any) {
    this.exportCompleted = false;
    const load$ = this.accountExportFacade.loadFile(ExportTypesEnum.Xls, 0).pipe(takeUntil(this.onDestroy$));
    const stateUpdate$ = this.isUpdatingExport$.pipe(
      filter(isUpdate => isUpdate === false && this.exportCompleted === false),
      mergeMap(() => this.accountExportFacade.get$(0)),
      takeUntil(this.onDestroy$));

    concat(load$, stateUpdate$).pipe(takeUntil(this.onDestroy$))
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((blob: Blob) => {
        // Prevent duplicates
        this.exportCompleted = true;
        const now = new Date();
        const fileName = `Account List ${now.toLocaleString()}.xls`;
        const objectUrl: string = window.URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;

        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        window.URL.revokeObjectURL(objectUrl);
      });

  }

  refresh(state: ClrDatagridStateInterface<AccountTableViewModel>) {
    // TODO: Sorting for second part of development flow

    this.searchSettings.Filter = [];
    if (state.filters) {
      for (const filter of state.filters) {
        const { property, value } = filter as {
          property: string;
          value: string;
        };
        this.searchSettings.Filter.push({
          // TODO: Another options
          CompareType: FilterType.Contains,
          Field: property,
          Value1: value,
          Value2: null
        });
      }
    }

    this.searchSettings.Pagination = {
      Page: state.page.current - 1,
      PerPage: state.page.size
    };

    this.reloadAccounts();
  }

  reloadAccounts() {
    this.addAccountModal = false;
    const load$ = this.accountsListFacade.load(this.searchSettings, 0).pipe(takeUntil(this.onDestroy$));
    const stateUpdate$ = this.isUpdating$.pipe(
      filter((isUpdate) => isUpdate === false),
      mergeMap(() => this.accountsListFacade.get$(0)),
      takeUntil(this.onDestroy$));
    this.loading = true;
    concat(load$, stateUpdate$)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((res: AccountListViewModel) => {
        this.accountsList = res.resultList;
        this.total = res.total;
        this.loading = false;
      });
  }

  onAddAccountOk(data) {
    if (data === null) {
      this.addAccountModal = false;
    } else if (data.accountId > 0) {
      this.selected = data;
      this.addAccountModal = false;
      this.reloadAccounts();
      this.selectedChanged();
    }

  }

  selectedChanged() {
    const url = `/account/${this.selected.accountId}`;
    window.open(url, '_blank');
  }
}
