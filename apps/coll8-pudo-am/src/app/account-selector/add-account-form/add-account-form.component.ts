import { AccountCreateViewModel } from './../../models/AccountCreateViewModel';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { AccountTableViewModel } from './../../models/AccountTableViewModel';
import { AccountFacade } from './../../services/facades/AccountFacade';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { FormEditorComponent } from '../../shared/form-editor/form-editor.component';
import { PopupService } from '../../popup/popup.service';
import { take } from 'rxjs/operators';
import { ListFacade } from '../../services/facades/ListFacade';

@Component({
  selector: 'app-add-account-form',
  templateUrl: './add-account-form.component.html',
  styleUrls: ['./add-account-form.component.scss']
})
export class AddAccountFormComponent implements OnInit {
  accountForm: FormGroup;
  facade: AccountFacade;
  listFacade: ListFacade;
  selectedAccount: BehaviorSubject<AccountTableViewModel> = new BehaviorSubject(new AccountTableViewModel());

  @ViewChild(FormEditorComponent, { static: true }) childFormEditor: FormEditorComponent;

  constructor(
    private facadeInject: AccountFacade,
    private formBuilder: RxFormBuilder,
    private listFacadeInject: ListFacade,
    private popupService: PopupService
  ) {
    this.facade = this.facadeInject;
    this.listFacade = this.listFacadeInject;
  }

  ngOnInit() {
    this.accountForm = this.formBuilder.formGroup(new AccountCreateViewModel());
    this.listFacade.load$().pipe(take(1)).subscribe(isLoaded => {
      // possibly here error must be handled
    });
  }

  save(data) {
    this.popupService.closePopup(data);
  }

  close() {
    this.popupService.closePopup(null);
  }
}
