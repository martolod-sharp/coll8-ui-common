import { LocationResolverService } from './services/resolvers/location-resolver.service';
import { ModalConfirmComponent } from './shared/modal-confirm/modal-confirm.component';
import { LocationGuardService } from './services/guards/location-guard.service';
import { AccountGuardService } from './services/guards/account-guard.service';
import { AccountComponent } from './account/account.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MsalGuard } from '@azure/msal-angular/dist/msal-guard.service';
import { HomeMenuComponent } from './home-menu/home-menu.component';
import { LocationComponent } from './location/location.component';
import { PopupComponent } from './popup/popup.component';
import { AddAccountFormComponent } from './account-selector/add-account-form/add-account-form.component';
// tslint:disable-next-line: max-line-length
import { EditAccountContactFormComponent } from './account-contacts/account-contact/edit-account-contact-form/edit-account-contact-form.component';
import { PopupRoutes, PopupBaseRoute } from './popup/popup-routes';
import { AccountLocationModalComponent } from './account-locations/account-location-modal/account-location-modal.component';
// tslint:disable-next-line: max-line-length
import { AccountLocationOperatingHoursModalComponent } from './account-locations/account-location-operating-hours/account-location-operating-hours-modal/account-location-operating-hours-modal.component';
// tslint:disable-next-line: max-line-length
import { EditLocationContactFormComponent } from './location-contacts/location-contact/edit-location-contact-form/edit-location-contact-form.component';
import { ModalConfirmDeletionComponent } from './shared/modal-confirm-deletion/modal-confirm-deletion.component';
import { AccountResolverService } from './services/resolvers/account-resolver.service';


const mainRoutes: Routes = [
  // {
  //   path: '',
  //   component: AppComponent,
  //   canActivate: [ MsalGuard ]
  // },
  {
    path: '',
    component: HomeMenuComponent,
    canActivate: [ MsalGuard ]
  },
  {
    path: 'account/:id',
    component: AccountComponent,
    canActivate: [ MsalGuard ],
    resolve:{
      isAccountExsists: AccountResolverService
    }
  },
  {
    path: 'location/:id',
    component: LocationComponent,
    canActivate: [ MsalGuard ],
    resolve: {
      isLocationtExsists: LocationResolverService
    }
  }
];

const modalRoutes: Routes = [
  {
    path: PopupBaseRoute,
    component: PopupComponent,
    outlet: 'popup',
    children: [
      {
        path: PopupRoutes.AddAccount,
        component: AddAccountFormComponent,
      },
      {
        path: PopupRoutes.AccountContactAdd,
        component: EditAccountContactFormComponent,
      },
      {
        path: PopupRoutes.AccountContactEdit,
        component: EditAccountContactFormComponent,
      },
      {
        path: PopupRoutes.LocationAdd,
        component: AccountLocationModalComponent,
      },
      {
        path: PopupRoutes.OperatingHoursEdit,
        component: AccountLocationOperatingHoursModalComponent,
      },
      {
        path: PopupRoutes.LocationContactAdd,
        component: EditLocationContactFormComponent,
      },
      {
        path: PopupRoutes.LocationContactEdit,
        component: EditLocationContactFormComponent,
      },
      {
        path: PopupRoutes.DeleteConfirmation,
        component: ModalConfirmDeletionComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot([...mainRoutes, ...modalRoutes])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
