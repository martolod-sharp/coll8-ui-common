import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export enum HomeTabs { Locations, Accounts }

@Component({
  selector: 'app-home-menu',
  templateUrl: './home-menu.component.html',
  styleUrls: ['./home-menu.component.scss']
})
export class HomeMenuComponent implements OnInit {
  homeTabs = HomeTabs;
  activeTab = new BehaviorSubject<HomeTabs>(HomeTabs.Locations);

  constructor() { }

  ngOnInit() {
  }
}
