import { Subscription } from 'rxjs';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { AccountDetailsViewModel } from './../../models/AccountDetailsViewModel';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input, Injectable, OnDestroy, Output, EventEmitter } from '@angular/core';
import { AccountDetailsFacade } from '../../services/facades/AccountDetailsFacade';
import { ListFacade } from '../../services/facades/ListFacade';

// Component for editing
@Component({
  selector: 'app-account-details-form',
  templateUrl: './account-details-form.component.html',
  styleUrls: ['./account-details-form.component.scss']
})
export class AccountDetailsFormComponent implements OnInit, OnDestroy {
  @Input() id: number;
  accountForm: FormGroup;
  accountDetailsFacade: AccountDetailsFacade;
  listFacade: ListFacade;
  loadListSUbscription: Subscription;

  constructor(
    private accountDetailsFacadeInject: AccountDetailsFacade,
    private listFacadeInject: ListFacade,
    private fromBuilder: RxFormBuilder
  ) {
    this.accountForm = this.fromBuilder.formGroup(new AccountDetailsViewModel(this.id));
    this.accountDetailsFacade = this.accountDetailsFacadeInject;
    this.listFacade = this.listFacadeInject;
  }


  ngOnInit() {
   this.loadListSUbscription = this.listFacade.load$().subscribe(isLoaded => {
      // possibly here error must be handled
    });
  }

  ngOnDestroy(): void {
    this.loadListSUbscription.unsubscribe();
  }
}
