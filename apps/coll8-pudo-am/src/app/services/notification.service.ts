import { NotificationType } from './../models/NotificationItem';
import { NotificationItem } from '../models/NotificationItem';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private notification$ = new BehaviorSubject<NotificationItem>(null);

  notify(notification: NotificationItem): void {

      alert(notification.message);
      this.notification$.next(notification);
  }

   getNotification$(): Observable<NotificationItem> {
    return this.notification$.asObservable();
   }
}
