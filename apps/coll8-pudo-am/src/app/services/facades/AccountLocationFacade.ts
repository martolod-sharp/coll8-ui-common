import { NotificationService } from './../notification.service';
import { LocationViewModel } from '../../models/LocationViewModel';
import { Injectable } from '@angular/core';
import { AccountLocationApi } from '../api/AccountLocationApi';
import { BaseSingleObjectFacade } from './BaseSingleObjectFacade';
@Injectable({ providedIn: 'root' })
export class AccountLocationFacade extends BaseSingleObjectFacade<LocationViewModel> {
  constructor(api: AccountLocationApi, notificationService: NotificationService) { super(api, notificationService); }
}
