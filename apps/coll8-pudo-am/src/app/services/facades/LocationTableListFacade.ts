import { Injectable } from '@angular/core';
import { BaseTableCollectionFacade } from './BaseTableCollectionFacade';
import { LocationTableViewModel } from '../../models/LocationTableViewModel';
import { LocationTableListApi } from '../api/LocationTableListApi';
import { NotificationService } from '../notification.service';
@Injectable({ providedIn: 'root' })
export class LocationTableListFacade extends BaseTableCollectionFacade<LocationTableViewModel> {
  constructor(api: LocationTableListApi, notificationService: NotificationService) {
    super(api, notificationService);
  }
}
