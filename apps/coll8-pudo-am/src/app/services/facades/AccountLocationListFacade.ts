import { NotificationService } from './../notification.service';
import { LocationViewModel } from '../../models/LocationViewModel';
import { BaseCollectionFacade } from './BaseCollectionFacade';
import { Injectable } from '@angular/core';
import { AccountLocationApi } from '../api/AccountLocationApi';
@Injectable({ providedIn: 'root' })
export class AccountLocationListFacade extends BaseCollectionFacade<LocationViewModel> {
  constructor(api: AccountLocationApi, notificationService: NotificationService ) { super(api, notificationService); }
}
