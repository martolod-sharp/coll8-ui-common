import { NotificationService } from './../notification.service';
import { AccountContactViewModel } from '../../models/AccountContactViewModel';
import { Injectable } from '@angular/core';
import { AccountContactApi } from '../api/AccountContactApi';
import { BaseCollectionFacade } from './BaseCollectionFacade';
// todo create generic facade  for collections
@Injectable({ providedIn: 'root' })
export class ContactsListFacade extends BaseCollectionFacade<AccountContactViewModel> {
  constructor(api: AccountContactApi, notificationService: NotificationService) { super(api, notificationService); }

}
