import { NotificationService } from './../notification.service';
import { BaseCollectionFacade } from './BaseCollectionFacade';
import { Injectable } from '@angular/core';
import { LocationScheduleViewModel } from '../../models/LocationSchedule/LocationScheduleViewModel';
import { LocationSchedulesApi } from '../api/LocationSchedulesApi';
@Injectable({ providedIn: 'root' })
export class LocationSchedulesListFacade extends BaseCollectionFacade<LocationScheduleViewModel> {
  constructor(api: LocationSchedulesApi, notificationService: NotificationService) { super(api, notificationService); }
}


