import { NotificationService } from './../notification.service';
import { Injectable } from '@angular/core';
import { AccountTableViewModel } from '../../models/AccountTableViewModel';
import { AccountTableListApi } from '../api/AccountTableListApi';
import { BaseTableCollectionFacade } from './BaseTableCollectionFacade';
@Injectable({ providedIn: 'root' })
export class AccountTableListFacade extends BaseTableCollectionFacade<AccountTableViewModel> {
  constructor(api: AccountTableListApi, notificationService: NotificationService) {
    super(api, notificationService);
  }
}
