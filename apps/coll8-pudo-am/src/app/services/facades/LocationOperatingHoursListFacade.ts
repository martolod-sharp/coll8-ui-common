import { NotificationService } from './../notification.service';
import { LocationOperatingHoursApi } from './../api/LocatrionOperatingHoursApi';
import { BaseCollectionFacade } from './BaseCollectionFacade';
import { Injectable } from '@angular/core';
import { LocationOperatingHoursViewModel } from '../../models/LocationOperatingHoursViewModel';
@Injectable({ providedIn: 'root' })
export class LocationOperatingHoursListFacade extends BaseCollectionFacade<LocationOperatingHoursViewModel> {
  constructor(api: LocationOperatingHoursApi, notificationService: NotificationService) { super(api, notificationService); }
}


