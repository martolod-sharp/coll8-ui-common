import { BaseFacadeErrorHandler } from './BaseFacadeErroHandler';
import { NotAuthorizedError } from './../api/errors/NotAuthorizedError';
import { NotFoundError } from './../api/errors/NotFoundError';
import { NotificationService } from './../notification.service';
import { ServerUnavailableError } from './../api/errors/ServerUnavailableError';
import { ServerValidationError } from './../api/errors/ServerValidationError';
import { IErrorModel } from './../../models/Interfaces/IErrorModel';
import { tap, mergeMap, map, catchError } from 'rxjs/operators';
import { Observable, of, Subscription } from 'rxjs';
import { CrudApi } from '../api/CrudApi';
import { BaseState } from '../../shared/states/BaseState';
// Internal service, facade, that knows about STATE and API

export class BaseSingleObjectFacade<T> extends BaseFacadeErrorHandler {

  protected objectState: BaseState<T>;
  updateSubscribtions: Subscription[] = [];
  // ObjectFacade<T>
  constructor(
    private api: CrudApi<T>,
    notificationService: NotificationService
  ) {
    super(notificationService);
    this.objectState = new BaseState<T>();
  }

  prepareForCreate(model: T, id: number) {
    this.objectState.setUpdating(true, id);
    this.objectState.setData(model, id);
    this.objectState.setUpdating(false, id);
  }

  isUpdating$(id: number): Observable<boolean> {
    return this.objectState.isUpdating$(id);
  }
  get$(id: number): Observable<T> {
    return this.objectState.getData$(id);
  }

  load(id: number) {
    this.objectState.setUpdating(true, id);
    return this.makeRequest(id,this.api.get(id).pipe(
      tap(data => {
        this.objectState.setData(data, id);
        this.objectState.setUpdating(false, id);
      })));
  }

  delete(id: number) {
    this.objectState.setUpdating(true, id);
    return this.makeRequest(id, this.api.remove(id).pipe(
      tap(data => {
        this.objectState.setData(null, id);
        this.objectState.setUpdating(false, id);
      })));
  }

  update(model: T, id: number): Observable<IErrorModel> {
    this.objectState.setUpdating(true, id);
    const idkey = Object.keys(model)[0];
    if (this.updateSubscribtions[id] !== undefined) {
      this.updateSubscribtions[id].unsubscribe();
    }
    return this.makeRequest(id, (model[idkey] === 0
      ? this.api.create(model)
      : this.api.update(model)
    ).pipe(
      tap((newModel: T) => {
        this.objectState.setData(newModel, id);
        this.objectState.setData(newModel, newModel[idkey]);
        this.objectState.setUpdating(false, id);
      }),
      map(() => null)));
  }

  getId(): number {
    alert('You should call extender getId() function!');
    return 0;
  }

  getMasterId(): number {
    alert('You should call extender getMasterId() function!');
    return 0;
  }
}
