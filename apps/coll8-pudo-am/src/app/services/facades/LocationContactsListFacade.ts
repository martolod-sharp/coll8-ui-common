import { NotificationService } from './../notification.service';
import { Injectable } from '@angular/core';
import { BaseCollectionFacade } from './BaseCollectionFacade';
import { LocationContactViewModel } from '../../models/LocationContactViewModel';
import { LocationContactApi } from '../api/LocationContactApi';
// todo create generic facade  for collections
@Injectable({ providedIn: 'root' })
export class LocationContactsListFacade extends BaseCollectionFacade<LocationContactViewModel> {
  constructor(api: LocationContactApi, notificationService: NotificationService) { super(api, notificationService); }
}
