import { BaseState } from './../../shared/states/BaseState';
import { NotificationItem } from '../../models/NotificationItem';
import { NotificationService } from './../notification.service';
import { Observer, of, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ServerValidationError } from '../api/errors/ServerValidationError';

export class BaseFacadeErrorHandler {
  protected objectState: BaseState<any>;

  constructor(private notificationService: NotificationService) {}

  makeRequest(id: number , observer: Observable<any>): Observable<any> {
    return observer.pipe(catchError((err: NotificationItem) => {
      this.objectState.setUpdating(false, id);
      if (err instanceof ServerValidationError) {
        return of(err.model);
      } else {
        this.notificationService.notify(err);
        return of(null);
      }
    }));
  }
}
