import { VatRateViewModel } from '../../models/VatRateViewModel';
import { CurrencyViewModel } from '../../models/CurrencyViewModel';
import { CorporatedGroupViewModel } from '../../models/CorporatedGroupViewModel';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, forkJoin, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CountryViewModel } from '../../models/CountryViewModel';
import { CorporatedGroupApi, CountryApi, CurrencyApi, VatRateApi } from '../api/ListApi';

@Injectable({ providedIn: 'root' })
export class ListFacade {
  private updating$ = new BehaviorSubject<boolean>(false);
  private isLoaded = false;
  private groups: Array<CorporatedGroupViewModel>;
  private countries: Array<CountryViewModel>;
  private vatRates: Array<VatRateViewModel>;
  private currencies: Array<CurrencyViewModel>;

  private groupList; // $ = new BehaviorSubject<Array<ISelectOption<number>>>(new Array<ISelectOption<number>>());
  private countriesList; // $ = new BehaviorSubject<Array<ISelectOption<number>>>(new Array<ISelectOption<number>>());
  private vatRateList; // $ = new BehaviorSubject<Array<ISelectOption<number>>>(new Array<ISelectOption<number>>());
  private currenciesList; // $ = new BehaviorSubject<Array<ISelectOption<number>>>(new Array<ISelectOption<number>>());
  subscription: Subscription;

  constructor(private currencyApi: CurrencyApi, private vatRateApi: VatRateApi,
              private corporatedGroupApi: CorporatedGroupApi, private countryApi: CountryApi) {
    this.updating$.next(true);
  }
  load$(): Observable<boolean> {

    /// promice all
    const values$ = forkJoin([
      this.loadCorporatedGroups(),
      this.loadCoutriesList(),
      this.loadVatRateList(),
      this.loadCurrenciesList()
    ]);
    return new Observable<boolean>(subscriber => {
      if (!this.isLoaded) {
      this.subscription = values$.subscribe(() => {

          this.groupList = this.groups
            .map(item => ({ val: item.corporatedGroupId, name: item.name }));
          this.countriesList = this.countries
            .map(item => ({ val: item.countryId, name: item.countryCode + ' - ' + item.description }));
          this.vatRateList = this.vatRates
            .map(item => ({ val: item.vatRateId, name: item.vatCode }));

          this.currenciesList = this.currencies
            .map(item => ({ val: item.currencyId, name: item.currencyCode + ' - ' + item.description }));

          this.isLoaded = true;
          subscriber.next(true);
          subscriber.complete();
        });
      } else {
        if (this.subscription !== undefined) {
          this.subscription.unsubscribe();
        }
        subscriber.next(true);
        subscriber.complete();
      }
    });
  }
  loaded() {
    this.updating$.next(true);
  }
  loadCorporatedGroups() {
    return this.corporatedGroupApi.getAll().pipe(tap(list => {
      this.groups = list;
    }));
  }
  loadCoutriesList() {
    return this.countryApi.getAll().pipe(tap(list => {
      this.countries = list;
    }));
  }
  loadVatRateList() {
    return this.vatRateApi.getAll().pipe(tap(list => {
      this.vatRates = list;
    }));
  }
  loadCurrenciesList() {
    return this.currencyApi.getAll().pipe(tap(list => {
      this.currencies = list;
    }));
  }
  getCorporatedGroups() {
    return this.groupList;
  }
  getCountries() {
    return this.countriesList;
  }
  getVatRates() {
    return this.vatRateList;
  }
  getCurrencies() {
    return this.currenciesList;
  }
}
