import { NotificationService } from './../notification.service';
import { Injectable } from '@angular/core';
import { AccountContactViewModel } from '../../models/AccountContactViewModel';
import { AccountContactApi } from '../api/AccountContactApi';
import { BaseSingleObjectFacade } from './BaseSingleObjectFacade';
import { LocationContactViewModel } from '../../models/LocationContactViewModel';
import { LocationContactApi } from '../api/LocationContactApi';
@Injectable({ providedIn: 'root' })
export class LocationContactFacade extends BaseSingleObjectFacade<LocationContactViewModel> {
  constructor(api: LocationContactApi, notificationService: NotificationService) {
    super(api, notificationService);
  }
}
