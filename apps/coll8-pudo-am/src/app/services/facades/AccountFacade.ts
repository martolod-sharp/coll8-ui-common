import { NotificationService } from './../notification.service';
import { AccountViewModel } from './../../models/AccountViewModel';
import { Injectable } from '@angular/core';
import { BaseSingleObjectFacade } from './BaseSingleObjectFacade';
import { AccountApi } from '../api/AccountApi';
@Injectable({ providedIn: 'root' })
export class AccountFacade extends BaseSingleObjectFacade<AccountViewModel> {
  constructor(api: AccountApi, notificationService: NotificationService) {
    super(api,notificationService);
  }
}
