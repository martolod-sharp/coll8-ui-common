import { NotificationService } from './../notification.service';
import { Injectable } from '@angular/core';
import { BaseSingleObjectFacade } from './BaseSingleObjectFacade';
import { LocationScheduleViewModel } from '../../models/LocationSchedule/LocationScheduleViewModel';
import { LocationSchedulesApi } from '../api/LocationSchedulesApi';
@Injectable({ providedIn: 'root' })
export class LocationScheduleFacade extends BaseSingleObjectFacade<LocationScheduleViewModel> {
  constructor(api: LocationSchedulesApi, notificationService: NotificationService) { super(api, notificationService); }
}
