import { NotificationService } from './../notification.service';
import { Injectable } from '@angular/core';
import { AccountDetailsViewModel } from '../../models';
import { AccountDetailsApi } from '../api/AccountDetailsApi';
import { BaseSingleObjectFacade } from './BaseSingleObjectFacade';
@Injectable({ providedIn: 'root' })
export class AccountDetailsFacade extends BaseSingleObjectFacade<AccountDetailsViewModel> {
  constructor(api: AccountDetailsApi, notificationService: NotificationService) {
    super(api, notificationService);
  }
}
