import { Injectable } from '@angular/core';
import { BaseBlobFacade } from './BaseBlobFacade';
import { LocationExportApi } from '../api/LocationExportApi';
import { NotificationService } from '../notification.service';
@Injectable({ providedIn: 'root' })
export class LocationExportFacade extends BaseBlobFacade {
  constructor(api: LocationExportApi, notificationService: NotificationService) {
    super(api, notificationService);
  }
}
