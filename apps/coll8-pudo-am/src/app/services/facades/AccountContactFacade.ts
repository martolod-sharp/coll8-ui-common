import { NotificationService } from './../notification.service';
import { Injectable } from '@angular/core';
import { AccountContactViewModel } from '../../models/AccountContactViewModel';
import { AccountContactApi } from '../api/AccountContactApi';
import { BaseSingleObjectFacade } from './BaseSingleObjectFacade';
@Injectable({ providedIn: 'root' })
export class AccountContactFacade extends BaseSingleObjectFacade<AccountContactViewModel> {
  constructor(api: AccountContactApi, notificationService: NotificationService) {
    super(api, notificationService);
  }
}
