import { NotificationService } from './../notification.service';
import { Injectable } from '@angular/core';
import { AccountAddressViewModel } from '../../models/AccountAddressViewModel';
import { AccountAddressApi } from '../api/AccountAddressApi';
import { BaseSingleObjectFacade } from './BaseSingleObjectFacade';
@Injectable({ providedIn: 'root' })
export class AccountAddressFacade extends BaseSingleObjectFacade<AccountAddressViewModel> {
  constructor(api: AccountAddressApi, notificationService: NotificationService) {
    super(api, notificationService);
  }
}

