import { BaseFacadeErrorHandler } from './BaseFacadeErroHandler';
import { NotificationService } from './../notification.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BaseState } from '../../shared/states/BaseState';
import { CrudApi } from '../api/CrudApi';

export class BaseBlobFacade extends BaseFacadeErrorHandler {

    protected objectState: BaseState<Blob>;
    constructor(private api: CrudApi<Blob>, notificationService: NotificationService) {
      super(notificationService);
      this.objectState = new BaseState<Blob>();
    }

    isUpdating$(id: number): Observable<boolean> {
        return this.objectState.isUpdating$(id);
    }
    get$(id: number): Observable<Blob> {
        return this.objectState.getData$(id);
    }


    loadFile(docType: number, id: number): Observable<Blob> {
        this.objectState.setUpdating(true, id);
        return this.makeRequest(id, this.api.getFile(docType).pipe(tap(data => {
            this.objectState.setData(data, id);
            this.objectState.setUpdating(false, id);
        })));
    }
}
