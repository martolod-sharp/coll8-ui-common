import { NotificationService } from './../notification.service';
import { Injectable } from '@angular/core';
import { AccountExportApi } from '../api/AccountExportApi';
import { BaseBlobFacade } from './BaseBlobFacade';
@Injectable({ providedIn: 'root' })
export class AccountExportFacade extends BaseBlobFacade {
  constructor(api: AccountExportApi, notificationService: NotificationService) { super(api, notificationService); }
}
