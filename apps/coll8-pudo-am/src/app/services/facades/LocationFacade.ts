import { Injectable } from '@angular/core';
import { BaseSingleObjectFacade } from './BaseSingleObjectFacade';
import { LocationViewModel } from '../../models/LocationViewModel';
import { LocationApi } from '../api/LocationApi';
import { NotificationService } from '../notification.service';
@Injectable({ providedIn: 'root' })
export class LocationFacade extends BaseSingleObjectFacade<LocationViewModel> {
  constructor(api: LocationApi, notificationService: NotificationService) {
    super(api, notificationService);
  }
}
