import { NotificationService } from './../notification.service';
import { BaseFacadeErrorHandler } from './BaseFacadeErroHandler';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { TableSearchSettings } from '../../models/TableSearchSettings';
import { BaseState } from '../../shared/states/BaseState';
import { BaseListViewModel } from '../../models/Base/BaseListViewModel';
import { CrudApi } from '../api/CrudApi';

export class BaseTableCollectionFacade<T> extends BaseFacadeErrorHandler {

  protected objectState: BaseState<BaseListViewModel<T>>;
  constructor(private api: CrudApi<T>, notificationService: NotificationService) {
    super(notificationService);
    this.objectState = new BaseState<BaseListViewModel<T>>();
  }

  isUpdating$(id: number): Observable<boolean> {
    return this.objectState.isUpdating$(id);
  }
  get$(id: number): Observable<BaseListViewModel<T>> {
    return this.objectState.getData$(id);
  }

  load(settings: TableSearchSettings, id: number): Observable<BaseListViewModel<T>>  {
    this.objectState.setUpdating(true, id);
    return this.makeRequest(id, this.api.getTableList(settings).pipe(tap(data => {
      this.objectState.setData(data, id);
      this.objectState.setUpdating(false, id);
    })));
  }
}
