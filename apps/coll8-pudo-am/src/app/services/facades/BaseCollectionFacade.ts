import { BaseFacadeErrorHandler } from './BaseFacadeErroHandler';
import { NotificationService } from './../notification.service';
import { CrudApi } from '../api/CrudApi';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BaseCollectionState } from '../../shared/states/BaseCollectionState';
// todo create generic facade  for collections
export class BaseCollectionFacade<T extends object> extends BaseFacadeErrorHandler  {
  protected objectState: BaseCollectionState<T>;
  constructor(private api: CrudApi<T>,  notificationService: NotificationService) {
    super(notificationService);
    this.objectState = new BaseCollectionState<T>();
  }
  isUpdating$(id: number): Observable<boolean> {
    return this.objectState.isUpdating$(id);
  }
  get$(id: number): Observable<Array<T>> {
    return this.objectState.getData$(id);
  }
  load$(masterId: number) {
    this.objectState.setUpdating(true, masterId);
    return this.makeRequest(masterId, this.api.getAll(masterId).pipe(tap(data => {
      this.objectState.setData(data, masterId);
      this.objectState.setUpdating(false, masterId);
    })));
  }

  load(masterId: number) {
    return this.load$(masterId);
  }
  // just update state and return current collection
  add$(item: T, id: number): Observable<Array<T>> {
    this.objectState.setUpdating(true, id);
    this.objectState.addItem(item, id);
    this.objectState.setUpdating(false, id);
    return this.objectState.getData$(id);
  }
  // just update state and return current collection
  update$(item: T, id: number): Observable<Array<T>> {
    this.objectState.setUpdating(true, id);
    this.objectState.updateItem(item, id);
    this.objectState.setUpdating(false, id);
    return this.objectState.getData$(id);
  }
  // just update state and return current collection
  remove$(item: T, id: number): Observable<Array<T>> {
    this.objectState.setUpdating(true, id);
    this.objectState.removeItem(item, id);
    this.objectState.setUpdating(false, id);
    return this.objectState.getData$(id);
  }

  getId(): number {
    alert('You should call extender getId() function!');
    return 0;
  }

  getMasterId(): number {
    alert('You should call extender getMasterId() function!');
    return 0;
  }
}
