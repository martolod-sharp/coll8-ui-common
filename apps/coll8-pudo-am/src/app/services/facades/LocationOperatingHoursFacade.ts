import { NotificationService } from './../notification.service';
import { LocationOperatingHoursApi } from './../api/LocatrionOperatingHoursApi';
import { Injectable } from '@angular/core';
import { LocationOperatingHoursViewModel } from '../../models/LocationOperatingHoursViewModel';
import { BaseSingleObjectFacade } from './BaseSingleObjectFacade';
@Injectable({ providedIn: 'root' })
export class LocationOperatingHoursFacade extends BaseSingleObjectFacade<LocationOperatingHoursViewModel> {
  constructor(api: LocationOperatingHoursApi, notificationService: NotificationService) { super(api, notificationService); }
}
