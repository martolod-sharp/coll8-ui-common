import { CrudApi } from './CrudApi';
import { AccountTableViewModel } from '../../models/AccountTableViewModel';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })
export class AccountTableListApi extends CrudApi<AccountTableViewModel> {
  constructor(http: HttpClient) {
    super(http);
    this.getTableUri = environment.baseUrl + '/account/list';
  }

}



