import { NotFoundError } from './errors/NotFoundError';
import { ServerValidationError } from './errors/ServerValidationError';
import { catchError } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BaseListViewModel } from '../../models/Base/BaseListViewModel';
import { TableSearchSettings } from '../../models/TableSearchSettings';
import { ServerUnavailableError } from './errors/ServerUnavailableError';
import { NotAuthorizedError } from './errors/NotAuthorizedError';

// api service
// @Injectable({ providedIn: 'root' })
export class CrudApi<T> {
  protected getUri: string;
  protected getAllUri: string;
  protected getTableUri: string;
  protected getFileUri: string;
  protected updateUri: string;
  protected deleteUri: string;
  protected createUri: string;

  constructor(private http: HttpClient) {}

  private erroHandler = (ex: HttpErrorResponse) => {
    console.log({ex});
    switch (ex.status) {
      case 400:
        if ( 'errors' in ex.error) {
          return throwError(new ServerValidationError(ex.error));
        } else {
          return throwError(new ServerUnavailableError());
        }
      case 401:
        return throwError(new NotAuthorizedError());
      case 500:
        return throwError(new ServerUnavailableError());
      case 404:
        return throwError(new NotFoundError());
      case 500:
        return throwError(new ServerUnavailableError());
      default:
        return throwError(new ServerUnavailableError());
    }
  }

  notInitiatedAlert(uri: string) {
    if (!uri) {
      alert(
        'DEVELOPER!!! API is not initialized!!! Function init() should be called first'
      );
    }
  }
  get(id: number): Observable<T> {
    this.notInitiatedAlert(this.getUri);
    const uri = this.getUri.replace('{id}', id.toString());
    return this.http.get<T>(uri)
    .pipe(catchError(this.erroHandler)); // TODO: move to separated service that works with headers
  }

  update(model: T): Observable<T> {
    this.notInitiatedAlert(this.updateUri);
    return this.http
      .put<T>(this.updateUri, model)
      .pipe(catchError(this.erroHandler));
  }

  remove(id: number): Observable<object> {
    this.notInitiatedAlert(this.deleteUri);
    const uri = this.deleteUri.replace('{id}', id.toString());
    return this.http.delete(uri).pipe(catchError(this.erroHandler));
  }
  create(model: T): Observable<T> {
    this.notInitiatedAlert(this.createUri);
    return this.http
      .post<T>(this.createUri, model)
      .pipe(catchError(this.erroHandler));
  }
  getAll(masterId: number = 0): Observable<Array<T>> {
    this.notInitiatedAlert(this.getAllUri);
    const uri =
      masterId === 0
        ? this.getAllUri
        : this.getAllUri.replace('{id}', masterId.toString());
    return this.http.get<Array<T>>(uri).pipe(catchError(this.erroHandler)); // TODO: move to separated service that works with headers
  }

  getFile(docType: number): Observable<Blob> {
    this.notInitiatedAlert(this.getFileUri);

    return this.http
      .get(this.getFileUri, { responseType: 'blob' })
      .pipe(catchError(this.erroHandler));
  }

  getTableList(
    settings: TableSearchSettings
  ): Observable<BaseListViewModel<T>> {
    this.notInitiatedAlert(this.getTableUri);

    if (settings.Filter == null) {
      settings.Filter = [];
    }
    if (settings.Sort == null) {
      settings.Sort = [];
    }
    const json = JSON.stringify(settings);
    const encoded = encodeURIComponent(json);
    return this.http
      .get<BaseListViewModel<T>>(
        this.getTableUri + '?searchsettings=' + encoded
      )
      .pipe(catchError(this.erroHandler));
  }
}
