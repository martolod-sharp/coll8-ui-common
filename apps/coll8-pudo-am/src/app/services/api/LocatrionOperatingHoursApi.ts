import { LocationViewModel } from './../../models/LocationViewModel';
import { CrudApi } from './CrudApi';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocationOperatingHoursViewModel } from '../../models/LocationOperatingHoursViewModel';
import { environment } from 'apps/coll8-pudo-am/src/environments/environment';

@Injectable({ providedIn: 'root' })
export class LocationOperatingHoursApi extends CrudApi<LocationOperatingHoursViewModel> {

  constructor(http: HttpClient) {
    super(http);
    this.getUri = environment.baseUrl + '/location/operating-hours/{id}';
    this.createUri = environment.baseUrl + '/location/operating-hours/';
    this.updateUri = environment.baseUrl + '/location/operating-hours/';
    this.deleteUri = environment.baseUrl + '/location/operating-hours/{id}'; // {id} here is location Id (LocationId)
    this.getAllUri = environment.baseUrl + '/location/operating-hours/list/{id}'; // {id} here is master Id (LocationId)
  }
}
