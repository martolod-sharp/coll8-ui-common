import { CrudApi } from './CrudApi';
import { AccountDetailsViewModel } from '../../models';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AccountDetailsApi extends CrudApi<AccountDetailsViewModel> {
  constructor(http: HttpClient) {
    super(http);
    this.getUri = environment.baseUrl + '/account/{id}';
    this.createUri = environment.baseUrl + '/account/';
    this.updateUri = environment.baseUrl + '/account/details/';
    this.deleteUri = ''; // No Account Deletion by specification
  }

}
