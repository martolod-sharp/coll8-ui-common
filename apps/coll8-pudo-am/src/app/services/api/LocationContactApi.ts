import { CrudApi } from './CrudApi';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { LocationContactViewModel } from '../../models/LocationContactViewModel';

@Injectable({ providedIn: 'root' })
export class LocationContactApi extends CrudApi<LocationContactViewModel> {
 // private getAllUri = environment.baseUrl + '/contacts/account/list/{id}'; // {id} here is master Id (AccountId)
  constructor(http: HttpClient) {
    super(http);
    this.getUri = environment.baseUrl + '/contacts/location/{id}';
    this.createUri = environment.baseUrl + '/contacts/location/';
    this.updateUri = environment.baseUrl + '/contacts/location/';
    this.deleteUri = environment.baseUrl + '/contacts/location/{id}'; // {id} here is contact Id (ContactId)
    this.getAllUri = environment.baseUrl + '/contacts/location/list/{id}'; // {id} here is master Id (AccountId)
  }
}
