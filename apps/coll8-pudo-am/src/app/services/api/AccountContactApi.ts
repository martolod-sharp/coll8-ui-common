import { CrudApi } from './CrudApi';
import { HttpClient } from '@angular/common/http';
import { AccountContactViewModel } from '../../models/AccountContactViewModel';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AccountContactApi extends CrudApi<AccountContactViewModel> {
 // private getAllUri = environment.baseUrl + '/contacts/account/list/{id}'; // {id} here is master Id (AccountId)
  constructor(http: HttpClient) {
    super(http);
    this.getUri = environment.baseUrl + '/contacts/account/{id}';
    this.createUri = environment.baseUrl + '/contacts/account/';
    this.updateUri = environment.baseUrl + '/contacts/account/';
    this.deleteUri = environment.baseUrl + '/contacts/account/{id}'; // {id} here is contact Id (ContactId)
    this.getAllUri = environment.baseUrl + '/contacts/account/list/{id}'; // {id} here is master Id (AccountId)
  }
}
