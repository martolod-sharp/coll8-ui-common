import { CrudApi } from './CrudApi';
import { AccountTableViewModel } from '../../models/AccountTableViewModel';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { LocationTableViewModel } from '../../models/LocationTableViewModel';


@Injectable({ providedIn: 'root' })
export class LocationTableListApi extends CrudApi<LocationTableViewModel> {
  constructor(http: HttpClient) {
    super(http);
    this.getTableUri = environment.baseUrl + '/location/list';
  }

}



