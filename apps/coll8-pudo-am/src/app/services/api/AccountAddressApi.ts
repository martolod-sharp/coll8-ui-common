import { CrudApi } from './CrudApi';
import { HttpClient } from '@angular/common/http';
import { AccountAddressViewModel } from '../../models/AccountAddressViewModel';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AccountAddressApi extends CrudApi<AccountAddressViewModel> {
  constructor(http: HttpClient) {
    super(http);
    this.getUri = environment.baseUrl + '/account/{id}/address';
    this.createUri = ''; // AccountAddress is 1-to-1 with Account and created on Account creation
    this.updateUri = environment.baseUrl + '/account/address/';
    this.deleteUri = ''; // No AccountAddress Deletion by specification
  }
}
