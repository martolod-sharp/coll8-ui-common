import { CrudApi } from './CrudApi';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })
export class AccountExportApi extends CrudApi<Blob> {
  constructor(http: HttpClient) {
    super(http);
    this.getFileUri = environment.baseUrl + '/account/list/export';
  }

}



