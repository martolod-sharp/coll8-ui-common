import { AccountViewModel } from './../../models/AccountViewModel';
import { CrudApi } from './CrudApi';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AccountApi extends CrudApi<AccountViewModel> {
  constructor(http: HttpClient) {
    super(http);
    this.getUri = environment.baseUrl + '/account/{id}';
    this.createUri = environment.baseUrl + '/account/';
  }
}
