import { LocationViewModel } from './../../models/LocationViewModel';
import { CrudApi } from './CrudApi';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AccountLocationApi extends CrudApi<LocationViewModel> {

  constructor(http: HttpClient) {
    super(http);
    this.getUri = environment.baseUrl + '/location/{id}';
    this.createUri = environment.baseUrl + '/location/';
    this.updateUri = environment.baseUrl + '/location/';
    this.deleteUri = environment.baseUrl + '/location/{id}'; // {id} here is location Id (ContactId)
    this.getAllUri = environment.baseUrl + '/location/list/{id}'; // {id} here is master Id (AccountId)
  }
}
