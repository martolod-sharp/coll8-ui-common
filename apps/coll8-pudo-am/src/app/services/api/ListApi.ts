import { VatRateViewModel } from '../../models/VatRateViewModel';
import { CurrencyViewModel } from '../../models/CurrencyViewModel';
import { CorporatedGroupViewModel } from '../../models/CorporatedGroupViewModel';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CountryViewModel } from '../../models/CountryViewModel';
import { CrudApi } from './CrudApi';
import { environment } from '../../../environments/environment';

// api service
@Injectable({ providedIn: 'root' })
export class CorporatedGroupApi extends CrudApi<CorporatedGroupViewModel> {
  constructor(http: HttpClient) {
    super(http);
    this.getAllUri = environment.baseUrl + '/lists/corporated-group/';
  }
}

@Injectable({ providedIn: 'root' })
export class CountryApi extends CrudApi<CountryViewModel> {
  constructor(http: HttpClient) {
    super(http);
    this.getAllUri = environment.baseUrl + '/lists/country/';
  }
}

@Injectable({ providedIn: 'root' })
export class VatRateApi extends CrudApi<VatRateViewModel> {
  constructor(http: HttpClient) {
    super(http);
    this.getAllUri = environment.baseUrl + '/lists/vat-rate/';
  }
}

@Injectable({ providedIn: 'root' })
export class CurrencyApi extends CrudApi<CurrencyViewModel> {
  constructor(http: HttpClient) {
    super(http);
    this.getAllUri = environment.baseUrl + '/lists/currency/';
  }
}

// @Injectable({ providedIn: 'root' })
// export class ListApi {
  // readonly API = '/api/cashflowCategories';
  // constructor(private http: HttpClient) { }

  // getCurrenciesList(): Observable<Array<CurrencyViewModel>> {
  //   // return this.http.get<AccountDetails>(this.API);
  //   return new Observable<Array<CurrencyViewModel>>(subscribe => {
  //     subscribe.next([
  //       {
  //         CurrencyId: 1,
  //         CurrencyCode: 'dollar',
  //         Country: 'USA',
  //         Description: ''
  //       },
  //       {
  //         CurrencyId: 2,
  //         CurrencyCode: 'ruble',
  //         Country: 'Russia',
  //         Description: ''
  //       },
  //       { CurrencyId: 3, CurrencyCode: 'pound', Country: 'GB', Description: '' }
  //     ]);
  //     subscribe.complete();
  //   });
  // }
  // getVatRateList(): Observable<Array<VatRateViewModel>> {
  //   // return this.http.get<AccountDetails>(this.API);
  //   return new Observable<Array<VatRateViewModel>>(subscribe => {
  //     subscribe.next([
  //       {
  //         VatRateId: 1,
  //         CountryId: 0,
  //         VatCode: 'VatCode1',
  //         Description: '',
  //         VatRate1: 0,
  //         ActiveFrom: null,
  //         ActiveTo: null
  //       },
  //       {
  //         VatRateId: 2,
  //         CountryId: 0,
  //         VatCode: 'VatCode2',
  //         Description: '',
  //         VatRate1: 0,
  //         ActiveFrom: null,
  //         ActiveTo: null
  //       },
  //       {
  //         VatRateId: 3,
  //         CountryId: 0,
  //         VatCode: 'VatCode3',
  //         Description: '',
  //         VatRate1: 0,
  //         ActiveFrom: null,
  //         ActiveTo: null
  //       }
  //     ]);
  //     subscribe.complete();
  //   });
  // }
// }
