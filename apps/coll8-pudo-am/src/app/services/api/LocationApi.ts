import { CrudApi } from './CrudApi';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { LocationViewModel } from '../../models/LocationViewModel';
@Injectable({ providedIn: 'root' })
export class LocationApi extends CrudApi<LocationViewModel> {
  constructor(http: HttpClient) {
    super(http);
    this.getUri = environment.baseUrl + '/location/{id}';
    this.updateUri = environment.baseUrl + '/location/';
  }
}
