import { CrudApi } from './CrudApi';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { LocationScheduleViewModel } from '../../models/LocationSchedule/LocationScheduleViewModel';

@Injectable({ providedIn: 'root' })
export class LocationSchedulesApi extends CrudApi<LocationScheduleViewModel> {

  constructor(http: HttpClient) {
    super(http);
    this.updateUri = environment.baseUrl + '/schedules/location';
    this.getAllUri = environment.baseUrl + '/schedules/location/{id}'; // {id} here is master Id (LocationId)
  }
}
