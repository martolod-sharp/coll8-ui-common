import { NotificationType, NotificationItem } from '../../../models/NotificationItem';

export class ServerUnavailableError extends NotificationItem {

  constructor() {
    super(NotificationType.Danger, 'Server is unavailable. Please try again later.', 'ServerUnavailableError');
  }

}
