import { NotificationItem, NotificationType } from '../../../models/NotificationItem';
import { IErrorModel } from './../../../models/Interfaces/IErrorModel';

export class ServerValidationError extends NotificationItem {
  name = 'ServerValidationError';
  message: 'model is invalid';
  stack?: string;

  constructor(public model: IErrorModel) {
    super(NotificationType.Danger, 'model is invali', 'ServerValidationError')
  }

}
