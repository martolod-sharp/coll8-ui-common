import { NotificationType, NotificationItem } from '../../../models/NotificationItem';

export class NotAuthorizedError extends NotificationItem {

  constructor() {
    super(NotificationType.Danger, 'Oops, something went wrong. Please login again.', 'NotAuthorizedError');
  }

}
