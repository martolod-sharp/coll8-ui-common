import { NotificationType, NotificationItem } from '../../../models/NotificationItem';

export class NotFoundError extends NotificationItem {

  constructor() {
    super(NotificationType.Danger, 'Oops, something went wrong. Please try again later', 'NotFoundError');
  }

}

