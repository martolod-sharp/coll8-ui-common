import { LocationFacade } from '../facades/LocationFacade';
import { take, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocationGuardService implements CanActivate {

  constructor(private locaitonFacade: LocationFacade, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean >  {
   return this.locaitonFacade.load(route.params.id).pipe(take(1), tap((data) => {
    if (data === null) {
      this.router.navigate(['/']);
    }
  }), map((data) => data !==  null));
  }
}
