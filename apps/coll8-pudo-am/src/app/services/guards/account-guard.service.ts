import { take, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AccountFacade } from './../facades/AccountFacade';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AccountGuardService implements CanActivate {

  constructor(private accountFacade: AccountFacade, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean >  {
   return this.accountFacade.load(route.params.id).pipe(take(1), tap((data) => {
     if (data === null) {
       this.router.navigate(['/']);
     }
   }), map((data) => data !==  null));
  }
}
