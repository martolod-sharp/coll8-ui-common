import { take, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AccountFacade } from './../facades/AccountFacade';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AccountResolverService implements Resolve<boolean> {

  constructor(private accountFacade: AccountFacade) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean >  {
   return this.accountFacade.load(route.params.id).pipe(take(1), map((data) => data !==  null));
  }
}
