import { ConfirmActions } from './../shared/modal-confirm/ConfirmActions';
import { IConfirmParams } from './../shared/modal-confirm/IConfirmParams';
import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';
import { filter, map, take, startWith, switchMap } from 'rxjs/operators';

interface IsChangedState {
  [key: string]: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class CanLeaveService {

  private isChanged = new BehaviorSubject<IsChangedState>({});
  public warnModalConfig$ = new BehaviorSubject<IConfirmParams>({formName: '', canSave: false , resultAction: 0});
  public canLeave: boolean;

  constructor(
    private router: Router
  ) {
    this.router.events.pipe(
      filter(ev => ev instanceof NavigationEnd),
    ).subscribe(() => this.isChanged.next({}));

    this.canLeave$().subscribe(val => this.canLeave = val);
  }

  public canLeave$ = (ids: number[] = [], formNames: string[] = []) => this.isChanged.pipe(
      map(formsState => !Object.keys(formsState).
      filter((key: string) => (formNames.length === 0 || formNames.some(fname => key.startsWith(fname))) &&
       (ids.length === 0 || ids.some(id => key.endsWith(id.toString()))))
       .map(keyName => formsState[keyName]).some(isChanged => isChanged))
    )

  public setFormIsChanged(formName: string, formId: number, isChanged: boolean) {
    const currentState = this.isChanged.value;
    const key = `${formName}${formId}`;
    if (currentState[key] !== isChanged) {
      currentState[key] = isChanged;
      this.isChanged.next(currentState);
    }
  }

  showWarn(params: IConfirmParams ) {
    console.log('showWarn', {params});
    this.warnModalConfig$.next(params);
    return this.warnModalConfig$
    .pipe(
       filter((data: IConfirmParams) => data.resultAction !== undefined),
       take(1), switchMap((data: IConfirmParams) => of(data.resultAction)));
  }

  confirmAction(action: ConfirmActions) {
      const params = this.warnModalConfig$.value;
      this.warnModalConfig$.next({...params, resultAction: action});

  }

}
