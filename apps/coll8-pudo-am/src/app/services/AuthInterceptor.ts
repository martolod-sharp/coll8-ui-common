import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { MsalService } from '@azure/msal-angular';
import { mergeMap } from 'rxjs/operators';

class Authority {
    accessToken: string;
    idToken: string;
    expiresIn: number;
}

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private msalService: MsalService) { }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const scopes = this.msalService.getScopesForEndpoint(req.url);
        const user = this.msalService.getUser();
        let exp = Date.now();
        let msalToken: string = null;
        let tokenStorageKey: string;
        const keys = Object.keys(localStorage);
        keys.forEach(key => {
            if (key.indexOf(user.userIdentifier) !== -1) {
                tokenStorageKey = key;
                const authority: Authority = JSON.parse(localStorage.getItem(key));
                msalToken = authority.accessToken;
                exp = authority.expiresIn;
            }
        });


        if (msalToken === undefined || msalToken === null || Date.now() >= exp * 1000) {
            localStorage.removeItem(tokenStorageKey);
            // console.log('Token is invalid. AcquireTokenSilent will be executed.');
            return from(
                this.msalService.acquireTokenSilent(scopes)
                    .then((response: any) => {
                        const newToken: string = response;
                        // console.log('Trying to inject fresh token...');
                        // console.log('New token ends with:' + newToken.substring(newToken.length - 11, newToken.length - 1));
                        req = req.clone({
                            setHeaders: {
                                Authorization: `Bearer ${newToken}`
                            }
                        });

                        return req;
                    })
            )
                .pipe(
                    mergeMap(nextReq => next.handle(nextReq))
                );


        } else {
            // console.log('Trying to inject token...');
            // console.log('Token ends with:' + msalToken.substring(msalToken.length - 11, msalToken.length - 1));
            req = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${msalToken}`
                }
            });
            return next.handle(req);
        }
    }
}
