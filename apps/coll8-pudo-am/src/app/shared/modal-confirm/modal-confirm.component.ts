import { takeUntil, take } from 'rxjs/operators';
import { CanLeaveService } from '../../services/can-leave.service';
import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { IConfirmParams } from './IConfirmParams';
import { ConfirmActions } from './ConfirmActions';

@Component({
  selector: 'app-modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styleUrls: ['./modal-confirm.component.scss']
})
export class ModalConfirmComponent implements OnInit, OnDestroy {
  public isOpen = false;
  @Input() public set open(value: boolean) {
    this.isOpen = value;
  }
  private onDestroy$ = new Subject();
  @Input() formName: string;
  @Input() canSave: boolean;
  btnActions = ConfirmActions;

  constructor(public canLeaveService: CanLeaveService) { }

  ngOnInit() {
    this.canLeaveService.warnModalConfig$.pipe(takeUntil(this.onDestroy$))
    .subscribe( (data: IConfirmParams) => {
      this.canSave = data.canSave;
      this.formName = data.formName;
      this.isOpen = data.resultAction === undefined;
    });
  }

  click(e: Event, action: ConfirmActions) {
    e.stopPropagation();
    this.canLeaveService.confirmAction(action);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
