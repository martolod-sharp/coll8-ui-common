import { NgModule } from '@angular/core';
import { ModalConfirmComponent } from './modal-confirm.component';
import { CommonModule } from '@angular/common';
import { ClarityModule } from '@clr/angular';

@NgModule({
  declarations: [
    ModalConfirmComponent
  ],
  imports: [
    CommonModule,
    ClarityModule,
  ],
  exports: [
    ModalConfirmComponent
  ]
})
export class ModalConfirmModule { }
