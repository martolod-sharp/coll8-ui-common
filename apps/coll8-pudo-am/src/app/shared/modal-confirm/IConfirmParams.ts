import { ConfirmActions } from './ConfirmActions';
import { BehaviorSubject } from 'rxjs';
export interface IConfirmParams {
  formName: string;
  canSave: boolean;
  resultAction?: ConfirmActions;
}
