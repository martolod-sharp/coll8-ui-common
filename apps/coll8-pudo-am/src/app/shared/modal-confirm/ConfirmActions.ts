export enum ConfirmActions {
  SAVE,
  CANCEL,
  BACK_TO_EDITING
}
