import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { BaseSingleObjectFacade } from '../../services/facades/BaseSingleObjectFacade';
import { IContactViewModel } from '../../models/Interfaces/IContactViewModel';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
import { FormEditorComponent } from '../form-editor/form-editor.component';
import { BaseCollectionFacade } from '../../services/facades/BaseCollectionFacade';
import { takeUntil } from 'rxjs/operators';
import { PopupRoutes } from '../../popup/popup-routes';
import { PopupService } from '../../popup/popup.service';

@Component({
  selector: 'app-base-edit-contact-form',
  templateUrl: './base-edit-contact-form.component.html',
  styleUrls: ['./base-edit-contact-form.component.scss']
})
export class BaseEditContactFormComponent implements OnInit, OnDestroy {

  @Input() id = 0;

  @Input() accountId = 0; // Can be a Location Id for Location contacts use

  @Input() title: string;

  formName = 'Account Contacts';

  @ViewChild(FormEditorComponent, { static: true }) childFormEditor: FormEditorComponent;

  contactFrom: FormGroup;
  buildModel: IContactViewModel;
  facade: BaseSingleObjectFacade<IContactViewModel>;

  constructor(
    private facadeInject: BaseSingleObjectFacade<IContactViewModel>,
    private fromBuilder: RxFormBuilder,
    protected popupService: PopupService
  ) {
    this.facade = this.facadeInject;
  }

  ngOnInit() {
    this.contactFrom = this.fromBuilder.formGroup(this.buildModel.newWithParent(this.accountId));
  }

  ngOnDestroy() {
  }


  save(data) {
    this.popupService.closePopup(data);
  }

  close() {
    this.popupService.closePopup();
  }
}
