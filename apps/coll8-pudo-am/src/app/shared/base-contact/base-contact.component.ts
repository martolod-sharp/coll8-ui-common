import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { IContactViewModel } from '../../models/Interfaces/IContactViewModel';
import { BaseCollectionFacade } from '../../services/facades/BaseCollectionFacade';
import { filter, mergeMap, takeUntil } from 'rxjs/operators';
import { concat, Subscription, Subject } from 'rxjs';
import { PopupService } from '../../popup/popup.service';
import { PopupRoutes } from '../../popup/popup-routes';
import { CanLeaveService } from '../../services/can-leave.service';

@Component({
  selector: 'app-base-contact-component',
  templateUrl: './base-contact.component.html',
  styleUrls: ['./base-contact.component.scss']
})
export class BaseContactComponent implements OnInit, OnDestroy {
  type: string;
  items: Array<IContactViewModel>;
  editedContactId = 0;
  @Input() id: number;

  modalTitle = 'Add Contact';
  loadSubscription: Subscription;

  private onDestroy$ = new Subject();

  constructor(
    private conatactsListFasade: BaseCollectionFacade<IContactViewModel>,
    private popupService: PopupService,
    private canLeaveService: CanLeaveService
  ) { }


  ngOnInit() {
    // load component data
    const stateUpdate$ = this.conatactsListFasade.isUpdating$(this.id).pipe(
      filter((isUpdate) => isUpdate === false),
      mergeMap(() => this.conatactsListFasade.get$(this.id).pipe(takeUntil(this.onDestroy$))),
      takeUntil(this.onDestroy$));

    const load$ = this.conatactsListFasade.load$(this.id).
    pipe(mergeMap(() => stateUpdate$), takeUntil(this.onDestroy$));

    this.loadSubscription = concat(load$, stateUpdate$).
    pipe(takeUntil(this.onDestroy$)).subscribe(items => this.items = items);
  }

  editContact(conatct: IContactViewModel) {
    this.modalTitle = 'Edit Contact';
    this.editedContactId = conatct.contactId;
    const pupupRoute = this.type === 'location' ? PopupRoutes.LocationContactEdit : PopupRoutes.AccountContactEdit;
    const params = {
      id: this.editedContactId,
      accountId: this.id,
      type: this.type,
      modalFormType: pupupRoute,
    };

    this.popupService.openPopup$<IContactViewModel>(pupupRoute, this.modalTitle, params).
    pipe(takeUntil(this.onDestroy$)).subscribe((res) => this.updateCollection(res));
  }

  add() {
    this.modalTitle = 'Add Contact';
    this.editedContactId = 0;
    const pupupRoute = this.type === 'location' ? PopupRoutes.LocationContactAdd : PopupRoutes.AccountContactAdd;
    const params = {
      id: 0,
      accountId: this.id,
      type: this.type,
      modalFormType: pupupRoute,
    };

    this.popupService.openPopup$<IContactViewModel>(pupupRoute, this.modalTitle, params).
    pipe(takeUntil(this.onDestroy$)).
    subscribe((res) => this.updateCollection(res));
  }

  remove(contact: IContactViewModel) {
    this.conatactsListFasade.remove$(contact, this.id);
  }

  updateCollection(contact: IContactViewModel) {
    // debugger;
    if (contact !== null) {
      if (this.editedContactId === 0) {
        this.conatactsListFasade.add$(contact, this.id);
      } else {
        this.conatactsListFasade.update$(contact, this.id);
      }
    }

  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
