import { ConfirmActions } from './../modal-confirm/ConfirmActions';
import { IConfirmParams } from './../modal-confirm/IConfirmParams';
import { PopupService } from './../../popup/popup.service';
import { IErrorModel } from './../../models/Interfaces/IErrorModel';
import { compareObj } from '../../helpers/compare';
import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable, BehaviorSubject, of, Subscription, iif } from 'rxjs';
import { BaseSingleObjectFacade } from '../../services/facades/BaseSingleObjectFacade';
import { CanLeaveService } from '../../services/can-leave.service';
import { mergeMap, filter, tap, takeUntil, take } from 'rxjs/operators';
import { PopupRoutes } from '../../popup/popup-routes';

@Component({
  selector: 'app-form-editor',
  templateUrl: './form-editor.component.html',
  styleUrls: ['./form-editor.component.scss']
})
export class FormEditorComponent implements OnInit, OnDestroy {
  @Input() formName = '';
  @Input() id: number;
  @Input() formGroup: FormGroup;
  @Input() objectFacade: BaseSingleObjectFacade<object>;
  @Input() isModal = false;

  @Output() saveEvent = new EventEmitter();
  @Output() cancelEvent = new EventEmitter();

  isUpdating$: Observable<boolean>;
  @Input() editedObject: object;
  updatingSubscribtion: Subscription;
  modalStateSubscribtion: Subscription;
  storedIsChanged = false;
  inProcess = false;
  constructor(
    private canLeaveService: CanLeaveService
  ) {

    this.checkClickOutSide = this.checkClickOutSide.bind(this);
  }

  ngOnInit() {
    this.init();
  }

  init() {
    if (this.objectFacade !== undefined) {
      this.isUpdating$ = this.objectFacade.isUpdating$(this.id).pipe(tap((isUpdating) => this.inProcess = isUpdating));
      const stateUpdate$ = this.isUpdating$.pipe(
        filter((isUpdate) => isUpdate === false),
        mergeMap(() => this.objectFacade.get$(this.id), 1));

      const load$ = this.objectFacade.load(this.id).pipe(mergeMap(() => stateUpdate$, 1));
      if (this.updatingSubscribtion !== undefined) {
        this.updatingSubscribtion.unsubscribe();
      }
      this.updatingSubscribtion = iif(() => this.id > 0, load$, stateUpdate$)
        .subscribe(res => {
          if (res !== null) {
            this.editedObject = res;
            this.formGroup.setValue(res);
          }
        });

      if (this.id === 0) {
        this.objectFacade.prepareForCreate(this.formGroup.value, this.id);
      }



    }


  }

 get formId() { return `${this.formName.replace(' ', '_').toLowerCase()}${this.id}`; }

  canSave(): boolean {
    return this.isValid() && this.isChanged();
  }

  save(e: Event) {
    if (!this.inProcess && this.isValid()) {
      this.objectFacade.update(this.formGroup.value, this.id)
        .pipe(tap(err => {
          if (err === null) {
            this.saveEvent.emit(this.formGroup.value);
          } else {
            return err;
          }
        }),
          filter(err => err !== null))
        .subscribe(err => this.handleErrror(err));
    }
  }

  handleErrror(model: IErrorModel) {

    Object.keys(this.formGroup.controls)
      .filter(key => model.errors[key] !== undefined)
      .forEach(key => {
        const errors = {};
        model.errors[key].forEach((message, i) => {
          errors[key + i] = { message };
        });
        console.log({ errors });
        this.formGroup.controls[key].setErrors(errors);
      });
  }

  cancel(e: Event) {
    this.formGroup.markAsUntouched();
    this.formGroup.patchValue(this.editedObject);
    this.cancelEvent.emit(this.editedObject);
  }

  isValid(): boolean {
    return this.formGroup.valid;
  }

  showValidationErrors(e: Event) {
   if (e !== null && e !== undefined) {
     e.stopPropagation();
   }
   const elm =  document.getElementById(this.formId);
   if ( elm  !== null) {
    elm.scrollIntoView({ behavior: 'smooth', block: 'start' });
   }
  }

  // deprecated
  isEmpty(): boolean {
    console.warn('the method "isEmpty()" is deprecated!');
    return !Object.keys(this.formGroup.value).some(key => {
      // todo: add normal implementation of  ignored fields
      if (key.indexOf(`Id`) !== -1) {
        return false;
      }
      const value = this.formGroup.value[key];
      switch (typeof value) {
        case 'string':
          return value.length > 0;
        case 'number':
          return value !== 0;
        default:
          return false;
      }
    });
  }

  isChanged(): boolean {
    let isChanged = true;
    if ((this.editedObject === null || this.editedObject === undefined)
      || (this.formGroup.value === null || this.formGroup.value === undefined)) {

      isChanged = false;
    }
    // compareObj returns true if objects are identical and false if not
    isChanged = isChanged && !compareObj(this.editedObject, this.formGroup.value);
    this.canLeaveService.setFormIsChanged(this.formName, this.id, isChanged);
    return isChanged;
  }

  checkClickOutSide(e: Event) {
    e.stopPropagation();
    e.preventDefault();
    if (!this.isModal) {

      // Exit from function if modal already opened
      this.canLeaveService.showWarn({formName: this.formName, canSave: this.canSave()}).pipe(take(1))
      .subscribe((action: ConfirmActions) => {
        switch (action) {
          case ConfirmActions.SAVE:
            this.save(null);
            break;
          case ConfirmActions.CANCEL:
            this.cancel(null);
          break;
          case ConfirmActions.BACK_TO_EDITING:
            this.showValidationErrors(null);
            break;
          default:
            break;
        }
      });

      // this.formGroup.markAllAsTouched();
      // this.formGroup.setValue(this.formGroup.value);
    }
  }

  ngOnDestroy(): void {

    if (this.updatingSubscribtion !== undefined) {
      this.updatingSubscribtion.unsubscribe();
    }
    if (this.modalStateSubscribtion !== undefined) {
      this.modalStateSubscribtion.unsubscribe();
    }

  }
}
