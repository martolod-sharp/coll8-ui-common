import { FormArray } from '@angular/forms';
import { PopupService } from './../../popup/popup.service';
import { Component, Input } from '@angular/core';
import { CanLeaveService } from '../../services/can-leave.service';
import { FormEditorComponent } from './form-editor.component';
import { filter, mergeMap } from 'rxjs/operators';
import { iif } from 'rxjs';
import { RxFormBuilder } from '@rxweb/reactive-form-validators';
@Component({
  selector: 'app-form-collection-editor',
  templateUrl: './form-editor.component.html',
  styleUrls: ['./form-editor.component.scss']
})
export class FormCollectionEditorComponent extends FormEditorComponent {
  @Input() arrayControlName = null;
  constructor( canLeaveService: CanLeaveService,  popupService: PopupService, private fromBUilder: RxFormBuilder) {
    super(canLeaveService);
  }

  init() {
    if (this.objectFacade !== undefined) {
      this.isUpdating$ = this.objectFacade.isUpdating$(this.id);
      const stateUpdate$ = this.isUpdating$.pipe(
        filter((isUpdate) => isUpdate === false),
        mergeMap(() => this.objectFacade.get$(this.id), 1));

      const load$ = this.objectFacade.load(this.id).pipe(mergeMap(() => stateUpdate$, 1));
      if (this.updatingSubscribtion !== undefined) {
        this.updatingSubscribtion.unsubscribe();
      }
      this.updatingSubscribtion = iif(() => this.id > 0, load$, stateUpdate$)
        .subscribe((res: Array<any>) => {
          if (res !== null) {
            this.editedObject = { [this.arrayControlName]: res};
            const array = (this.formGroup.get(this.arrayControlName) as FormArray);
            array.clear();
            res.forEach(data => array.push(this.fromBUilder.group(data)));

            console.log({formGroup: this.formGroup.value, editedObject: this.editedObject});
          }
        });
    }


  }

  save(e: Event) {
    if (e !== null && e !== undefined) {
      e.stopPropagation();
    }
    if (this.isValid()) {
      this.saveEvent.emit(this.formGroup.value);
    }
  }

}
