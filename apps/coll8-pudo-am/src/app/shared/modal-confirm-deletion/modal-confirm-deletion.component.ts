import { Component } from '@angular/core';
import { PopupService } from '../../popup/popup.service';

@Component({
  selector: 'app-modal-confirm-deletion',
  templateUrl: './modal-confirm-deletion.component.html',
  styleUrls: ['./modal-confirm-deletion.component.scss']
})
export class ModalConfirmDeletionComponent {

  constructor(
    private popupService: PopupService
  ) { }

  confirm() {
    this.popupService.closePopup(true);
  }

  cansel() {
    this.popupService.closePopup(null);
  }
}
