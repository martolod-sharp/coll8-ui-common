import { BehaviorSubject } from 'rxjs';
export class BaseState<T> {
  private updating$: { [id: number]: BehaviorSubject<boolean> } = {};// =
  // { [0]: new BehaviorSubject<boolean>(false) }; // new BehaviorSubject<boolean>(false);
  // access to data from extended  object
  protected data$: { [id: number]: BehaviorSubject<T> } = {};// =
  // { [0]: new BehaviorSubject<T>(null)};

  constructor() {
    this.isUpdating$[0] = new BehaviorSubject<boolean>(false); // { [0]:  };
    this.data$[0] = new BehaviorSubject<T>(null);
  }

  isUpdating$(id: number) {
    if (this.updating$[id] === null || this.updating$[id] === undefined) {
      this.updating$[id] = new BehaviorSubject<boolean>(false);
    }
    return this.updating$[id].asObservable();
  }
  setUpdating(isUpdating: boolean, id: number) {
    if (this.updating$[id] === null || this.updating$[id] === undefined) {
      this.updating$[id] = new BehaviorSubject<boolean>(isUpdating);
    }
    this.updating$[id].next(isUpdating);
  }
  getData$(id: number) {
    if (this.data$[id] === null || this.data$[id] === undefined) {
      this.data$[id] = new BehaviorSubject<T>(null);
    }
    return this.data$[id].asObservable();
  }
  setData(newData: T, id: number) {
    if (this.data$[id] === null || this.data$[id] === undefined) {
      this.data$[id] = new BehaviorSubject<T>(newData);
    }
    this.data$[id].next(newData);
  }
}

// export class BaseState<T> {
//   private updating$ = new BehaviorSubject<boolean>(false);
//   // access to data from extended  object
//   protected data$ = new BehaviorSubject<T>(null);
//   isUpdating$() {
//     return this.updating$.asObservable();
//   }
//   setUpdating(isUpdating: boolean) {
//     this.updating$.next(isUpdating);
//   }
//   getData$() {
//     return this.data$.asObservable();
//   }
//   setData(newData: T) {
//     this.data$.next(newData);
//   }
// }
