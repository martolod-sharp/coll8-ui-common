import { BaseState } from './BaseState';
import { BehaviorSubject } from 'rxjs';
// base class for collection objecet
export class BaseCollectionState<T extends object> extends BaseState<Array<T>> {

  constructor() {
    super();
    // this.data$.next([]);
    this.data$ = {[0]: new BehaviorSubject<T[]>([])};
  }
  // add entity to collection
  addItem(item: T, id: number) {
    const items = this.data$[id].getValue();
    this.data$[id].next([...items, item]);
  }
  // update entity in collection
  updateItem(item: T, id: number) {
    const items = this.data$[id].getValue();
    const idField: string = Object.keys(item)[0];
    const index = items.findIndex(i => i[idField] === item[idField]);
    if (index > -1) {
      items[index] = item;
    }
    this.data$[id].next([...items]);
  }
  // remove entity from collection
  removeItem(item: T, id: number) {
    const items = this.data$[id].getValue();
    const idField: string = Object.keys(item)[0];
    this.data$[id].next(items.filter(i => i[idField] !== item[idField]));
  }
}
