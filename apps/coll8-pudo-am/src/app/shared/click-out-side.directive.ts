import { Directive, HostListener, ElementRef, Output, EventEmitter, OnInit, Input } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[clickOutSide]'
})

export class ClickOutSideDirective implements OnInit {
  constructor(private elem: ElementRef) { }
  @Input() isChanged: boolean;
  @Output() clickOutSideEvent: EventEmitter<any> = new EventEmitter<any>();
  ngOnInit() {
  }

  @HostListener('document:click', ['$event'])
  onMouseEnter(event: MouseEvent) {
    const target = event.target;
    const elem = this.elem.nativeElement;
    const isClickIn = elem.contains(target);
    if (!isClickIn && this.isChanged) {
      event.preventDefault();
      this.clickOutSideEvent.emit(event);
    }
  }

}
