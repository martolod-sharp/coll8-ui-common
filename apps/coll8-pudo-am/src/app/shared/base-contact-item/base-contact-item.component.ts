import { Subscription, Subject } from 'rxjs';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { IContactViewModel } from '../../models/Interfaces/IContactViewModel';
import { BaseSingleObjectFacade } from '../../services/facades/BaseSingleObjectFacade';
import { takeUntil } from 'rxjs/operators';
import { PopupService } from '../../popup/popup.service';
import { PopupRoutes } from '../../popup/popup-routes';

@Component({
  selector: 'app-base-contact-item',
  templateUrl: './base-contact-item.component.html',
  styleUrls: ['./base-contact-item.component.scss']
})
export class BaseContactItemComponent implements OnInit, OnDestroy {
  @Input() contact: IContactViewModel;
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onEdit = new EventEmitter<IContactViewModel>();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onDelete = new EventEmitter<IContactViewModel>();

  private onDestroy$ = new Subject();
  constructor(
    private facade: BaseSingleObjectFacade<IContactViewModel>,
    private popupService: PopupService
  ) { }

  ngOnInit() {

  }
  // transfer contant to  modal editor
  editContact() {
    // todo: maybe we should  open  edit modal from  here and emit it only to inform that  collection  has tobe updated ?
    this.onEdit.emit(this.contact);
  }

  // transfer contant to  modal confirm
  deleteContact() {
    this.popupService.openPopup$(PopupRoutes.DeleteConfirmation, 'Delete')
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(res => {
        if (res !== null) {
          this.onDelete.emit(this.contact);
        }
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
