import { BaseSingleObjectFacade } from './../../services/facades/BaseSingleObjectFacade';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-modal-editor',
  templateUrl: './modal-editor.component.html',
  styleUrls: ['./modal-editor.component.scss']
})
export class ModalEditorComponent implements OnInit {

  @Output() onClose = new EventEmitter();

  @Input() title: string;

  @Input()  data: object;

  @Input() formGroup: FormGroup;

  @Input() isOpen: boolean;

  @Input() id: number;

  @Input() objectFacade: BaseSingleObjectFacade<any>;

  constructor() { }

  ngOnInit() {
  }

  public handleChange(event) {
    this.onClose.emit(null);
  }

  close(data) {
    this.onClose.emit(data);
  }

}
