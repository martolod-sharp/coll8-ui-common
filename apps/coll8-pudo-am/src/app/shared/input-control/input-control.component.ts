import { Subject } from 'rxjs';
import { Component, OnInit, Input, ChangeDetectorRef, OnDestroy, AfterViewInit, AfterContentInit } from '@angular/core';
import {  AbstractControl } from '@angular/forms';
import { takeUntil, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-input-control',
  templateUrl: './input-control.component.html',
  styleUrls: ['./input-control.component.scss']
})
export class InputControlComponent implements OnInit, AfterContentInit, OnDestroy {

  @Input() label: string;
  @Input() placeholder: string;
  @Input() control: AbstractControl;
  @Input() readonly  = false;
  @Input() set disabled(val: boolean) {
     if (val) {
        this.control.disable();
     } else {
        this.control.enable();
     }
  }
  errors = [];
  isRequired = false;

  private onDestroy$ = new Subject();

  constructor(
    private cnahgeDetector: ChangeDetectorRef
  ) {}

  ngOnInit() {
    if (this.control.validator && {}.toString.call(this.control.validator) === '[object Function]') {
      const validators = this.control.validator(this.control);
      this.isRequired = !!(validators && !!validators.required);
    }

    this.control.statusChanges.pipe(
      startWith(null),
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      if (this.control.errors !== null) {
        this.errors = Object.keys(this.control.errors).map(errorName => {
          const { message, refValues } = this.control.errors[errorName];
          let result = message;
          if (refValues !== undefined) {
            refValues.forEach((val, i) => {
              result = message.replace(`@${i}`, val);
            });
          }
          return result;
        });
      } else {
        this.errors = [];
      }
    });
  }

  ngAfterContentInit() {
    this.cnahgeDetector.detectChanges();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
