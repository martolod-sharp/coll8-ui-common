import { InputControlComponent } from './../input-control/input-control.component';
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { AbstractControl } from '@angular/forms';

export interface ISelectOption<T> {
  val: T;
  name: string;
}

@Component({
  selector: 'app-select-control',
  templateUrl: './select-control.component.html',
  styleUrls: ['./select-control.component.scss']
})
export class SelectControlComponent extends InputControlComponent{
  @Input() placeholder: string;
  @Input() options: Array<ISelectOption<any>> = [];
  @Input() control: AbstractControl;
  isRequired = false;

  constructor(
    cnahgeDetector: ChangeDetectorRef
  ) {
    super(cnahgeDetector);
  }
}
