export function compareObj(obj1: object, obj2: object) {

    const isEqualKeyLength = Object.keys(obj1).length === Object.keys(obj2).length ;
    const isEqualProperties =  Object.keys(obj1)
    .every(key =>
       (obj1[key] !== undefined && obj1[key] !== null &&
        obj2[key] !== undefined && obj2[key] !== null &&
         typeof (obj1[key]) === 'object' && typeof(obj2[key]) === 'object') ?
        compareObj(obj1[key], obj2[key])
         : obj1[key] === obj2[key]);

    return isEqualKeyLength && isEqualProperties;
  }
