import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BroadcastService } from '@azure/msal-angular';
import { Router } from '@angular/router';
import { ReactiveFormConfig } from '@rxweb/reactive-form-validators';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'Coll8UI';

  isIframe = false;
  loggedIn = false;
  loginFailureSubscribtion: Subscription;
  loginSuccessSubscribtion: Subscription;
  acquireTokenSuccessSubscribtion: Subscription;
  acquireTokenFailureSuccessSubscribtion: Subscription;

  constructor(
    private broadcastService: BroadcastService,
    private router: Router
    ) {
    }


  ngOnInit() {
  this.loginFailureSubscribtion = this.broadcastService.subscribe('msal:loginFailure', this.onLoginOrTokenFailure.bind(this));

  this.loginSuccessSubscribtion = this.broadcastService.subscribe('msal:loginSuccess', (payload) => {});

  this.acquireTokenSuccessSubscribtion = this.broadcastService.subscribe('msal:acquireTokenSuccess', (payload) => {});

  this.acquireTokenFailureSuccessSubscribtion = this.broadcastService
  .subscribe('msal:acquireTokenFailure', this.onLoginOrTokenFailure.bind(this));

  ReactiveFormConfig.set({
      validationMessage: {
        required: 'This field is required.',
        maxLength: 'The length of this field shoulde be max @1 characters',
        email: 'Incorrect email format' ,
        time: 'The format of the string should be hh:mm ',
        phone: 'Incorrect phone format',
        longitude: 'Incorrect coordinate format was set. The example of the correct format: XX or XX.X',
        latitude: 'Incorrect coordinate format was set. The example of the correct format: XX or XX.X'
      }
  });
  }

  onLoginOrTokenFailure(payload) {
    this.clearMsalStorage();
    this.router.navigate(['/']);
  }

  clearMsalStorage() {
    const keys = Object.keys(localStorage);
    keys.forEach(key => {
      if (key.startsWith('msal') || key.indexOf(environment.adClientId) !== -1) {
        localStorage.removeItem(key);
      }
    });
  }

  ngOnDestroy(): void {
    this.loginFailureSubscribtion.unsubscribe();

    this.loginSuccessSubscribtion.unsubscribe();

    this.acquireTokenSuccessSubscribtion.unsubscribe();

    this.acquireTokenSuccessSubscribtion.unsubscribe();
  }
}
