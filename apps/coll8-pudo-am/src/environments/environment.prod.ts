export const environment = {
  production: true,
  baseUrl: 'https://coll8-api.stage.sharp-dev.net/api',
  adClientId: 'cee87721-cd83-4884-806e-7086286782f0',
  adClientScope: 'api://cee87721-cd83-4884-806e-7086286782f0/access_as_user',
  adCacheLocation: 'localStorage',
  adRedirectUri: 'https://coll8.stage.sharp-dev.net/'
};
