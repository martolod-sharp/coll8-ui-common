// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'https://coll8-api.stage.sharp-dev.net/api',
  adClientId: 'cee87721-cd83-4884-806e-7086286782f0',
  adClientScope: 'api://cee87721-cd83-4884-806e-7086286782f0/access_as_user',
  adCacheLocation: 'localStorage',
  adRedirectUri: 'http://localhost:4200/'

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
