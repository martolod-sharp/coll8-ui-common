module.exports = {
  name: 'coll8-pudo-am',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/coll8-pudo-am',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
