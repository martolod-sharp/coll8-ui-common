import { getGreeting } from '../support/app.po';

describe('coll8-pudo-am', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to coll8-pudo-am!');
  });
});
